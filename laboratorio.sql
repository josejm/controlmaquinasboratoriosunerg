-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2017 at 08:55 PM
-- Server version: 5.5.49-0+deb8u1
-- PHP Version: 5.6.27-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `laboratorio`
--
CREATE DATABASE IF NOT EXISTS `laboratorio` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `laboratorio`;

-- --------------------------------------------------------

--
-- Table structure for table `block_web`
--

DROP TABLE IF EXISTS `block_web`;
CREATE TABLE IF NOT EXISTS `block_web` (
`id` int(11) NOT NULL,
  `web` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_web`
--

INSERT INTO `block_web` (`id`, `web`) VALUES
(15, 'face');

-- --------------------------------------------------------

--
-- Table structure for table `estacion`
--

DROP TABLE IF EXISTS `estacion`;
CREATE TABLE IF NOT EXISTS `estacion` (
`id` int(11) NOT NULL,
  `id_laboratorio` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `ip` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estacion`
--

INSERT INTO `estacion` (`id`, `id_laboratorio`, `numero`, `ip`, `username`, `password`) VALUES
(11, 6, 1, '192.168.0.112', 'root', 'j2017m'),
(12, 7, 2, '192.168.0.150', 'joser', '192.168.0.150'),
(14, 6, 5, '192.168.0.111', 'mamapapa', 'r2015j'),
(15, 6, 4, '192.168.0.107', 'qwe', 'qwe');

-- --------------------------------------------------------

--
-- Table structure for table `historial_usuario`
--

DROP TABLE IF EXISTS `historial_usuario`;
CREATE TABLE IF NOT EXISTS `historial_usuario` (
`id` int(11) NOT NULL,
  `usuario` varchar(200) NOT NULL COMMENT 'no se utliza el id del usuario por que el usuario se puede eliminar el el historial quedaria incompleto',
  `laboratorio` varchar(200) NOT NULL COMMENT 'no se utiliza el id del laboratorio por que pueden eliminar el laboratorio',
  `maquina` varchar(200) NOT NULL COMMENT 'no se utiliza el id de la maquina por que pueden eliminar la maquina',
  `accion` varchar(200) NOT NULL COMMENT 'accion: INICIO DE SECCION , FIN DE SECCION',
  `fecha` datetime NOT NULL COMMENT 'fecha en que salio de la maquina'
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `historial_usuario`
--

INSERT INTO `historial_usuario` (`id`, `usuario`, `laboratorio`, `maquina`, `accion`, `fecha`) VALUES
(1, 'w', '1', '1', 'encender', '2017-05-06 00:00:00'),
(2, 'w', '1', '1', 'encender', '2017-05-03 03:12:30'),
(3, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:33:56'),
(4, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:41:55'),
(5, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:46:59'),
(6, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:02'),
(7, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:04'),
(8, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:12'),
(9, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:15'),
(10, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:31'),
(11, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:47:51'),
(12, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:48:02'),
(13, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:48:24'),
(14, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:48:44'),
(15, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:49:27'),
(16, 'w', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 12:49:38'),
(17, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:05:07'),
(18, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:18:04'),
(19, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:18:18'),
(20, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:21:02'),
(21, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:21:30'),
(22, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:22:02'),
(23, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:22:31'),
(24, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:22:51'),
(25, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:22:59'),
(26, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:27:34'),
(27, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:27:59'),
(28, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:28:48'),
(29, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:29:10'),
(30, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 13:33:13'),
(31, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:31:02'),
(32, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:32:03'),
(33, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:32:22'),
(34, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:33:16'),
(35, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:34:52'),
(36, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:35:34'),
(37, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:36:43'),
(38, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:36:56'),
(39, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:37:40'),
(40, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:40:16'),
(41, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:40:35'),
(42, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:42:31'),
(43, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:43:08'),
(44, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:43:38'),
(45, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:44:13'),
(46, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:45:26'),
(47, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:46:11'),
(48, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:46:24'),
(49, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:46:44'),
(50, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:47:53'),
(51, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:48:05'),
(52, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:49:40'),
(53, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:50:17'),
(54, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:53:04'),
(55, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:53:30'),
(56, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:54:57'),
(57, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:55:53'),
(58, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 19:57:50'),
(59, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:00:05'),
(60, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:05:57'),
(61, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:17:00'),
(62, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:17:55'),
(63, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:18:09'),
(64, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:20:31'),
(65, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:21:40'),
(66, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:23:13'),
(67, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:24:40'),
(68, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:25:26'),
(69, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:26:00'),
(70, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:26:32'),
(71, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:27:43'),
(72, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:30:16'),
(73, 'qwe', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-06 20:32:26'),
(74, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:34:35'),
(75, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:54:39'),
(76, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:54:59'),
(77, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:55:49'),
(78, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:56:00'),
(79, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:56:42'),
(80, '123', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 08:57:33'),
(81, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:16:42'),
(82, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:17:50'),
(83, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:19:08'),
(84, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:23:17'),
(85, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:23:48'),
(86, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:25:31'),
(87, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:33:12'),
(88, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:38:05'),
(89, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:38:53'),
(90, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:40:13'),
(91, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:46:27'),
(92, 'jose', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:49:14'),
(93, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:51:14'),
(94, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:51:40'),
(95, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 09:52:49'),
(96, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:02:29'),
(97, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:08:19'),
(98, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:11:15'),
(99, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:11:58'),
(100, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:12:42'),
(101, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:13:27'),
(102, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:14:34'),
(103, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:17:04'),
(104, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:17:14'),
(105, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:18:56'),
(106, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:19:04'),
(107, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:19:07'),
(108, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:19:08'),
(109, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:19:24'),
(110, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:22:56'),
(111, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:24:33'),
(112, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:25:53'),
(113, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:26:19'),
(114, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:26:59'),
(115, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:27:21'),
(116, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:31:51'),
(117, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:32:07'),
(118, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:32:25'),
(119, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:33:42'),
(120, 'asd', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:35:01'),
(121, 'jose', '1', '1', 'ENTRO EN LA MAQUINA: 1', '2017-05-07 10:35:15'),
(122, 'asd', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:24:08'),
(123, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:25:03'),
(124, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:26:12'),
(125, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:26:45'),
(126, 'asd', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:27:04'),
(127, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:27:07'),
(128, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:27:40'),
(129, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:27:43'),
(130, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:28:10'),
(131, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:28:17'),
(132, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:31:27'),
(133, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:34:06'),
(134, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:35:35'),
(135, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:35:39'),
(136, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:36:04'),
(137, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:37:02'),
(138, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:40:51'),
(139, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:41:34'),
(140, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 13:43:27'),
(141, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 13:43:34'),
(142, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:25:59'),
(143, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:29:36'),
(144, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:32:51'),
(145, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:34:12'),
(146, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 20:35:22'),
(147, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:40:00'),
(148, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 20:40:24'),
(149, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:40:30'),
(150, '', '1', '2', 'EL USUARIO HA CERRADO SECCION EN LA MAQUINA: 2', '2017-05-07 20:40:35'),
(151, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:40:39'),
(152, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-07 20:41:55'),
(153, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:09:10'),
(154, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:10:33'),
(155, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:12:42'),
(156, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:13:13'),
(157, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:13:44'),
(158, 'jose', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:14:04'),
(159, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 11:14:40'),
(160, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 18:10:57'),
(161, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 18:58:52'),
(162, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 18:59:22'),
(163, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:04:54'),
(164, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:05:10'),
(165, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:06:05'),
(166, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:06:31'),
(167, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:06:51'),
(168, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:08:41'),
(169, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:09:15'),
(170, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:09:42'),
(171, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:10:34'),
(172, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:16:18'),
(173, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:16:35'),
(174, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:17:03'),
(175, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:17:54'),
(176, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:18:27'),
(177, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:18:51'),
(178, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:19:46'),
(179, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:20:23'),
(180, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:21:42'),
(181, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:21:55'),
(182, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:22:20'),
(183, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:25:59'),
(184, 'qwe', '1', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:26:25'),
(185, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:28:23'),
(186, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:31:33'),
(187, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:32:14'),
(188, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:32:42'),
(189, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:32:55'),
(190, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:33:19'),
(191, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:41:24'),
(192, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:42:09'),
(193, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:42:54'),
(194, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:45:32'),
(195, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:47:01'),
(196, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:50:42'),
(197, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:58:20'),
(198, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 19:58:55'),
(199, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 19:59:23'),
(200, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 19:59:56'),
(201, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 20:00:10'),
(202, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 20:00:50'),
(203, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 20:01:55'),
(204, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 20:02:30'),
(205, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 20:02:36'),
(206, 'qwe', '2', '2', 'ENTRO EN LA MAQUINA: 2', '2017-05-14 20:03:03'),
(207, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 20:03:13'),
(208, 'Desconocido (Invitado)', '2', '2', 'Un usuario desconocido a iniciado sesion como invitado', '2017-05-14 20:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `laboratorio`
--

DROP TABLE IF EXISTS `laboratorio`;
CREATE TABLE IF NOT EXISTS `laboratorio` (
`id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `internet` tinyint(1) NOT NULL,
  `invitado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laboratorio`
--

INSERT INTO `laboratorio` (`id`, `numero`, `internet`, `invitado`) VALUES
(6, 1, 1, 0),
(7, 2, 1, 1),
(8, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(11) NOT NULL,
  `cedula` varchar(200) DEFAULT NULL,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `carpeta` varchar(100) NOT NULL,
  `seccion` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `cedula`, `user`, `password`, `carpeta`, `seccion`) VALUES
(1, NULL, 'w', 'w', '/2/2/2/2', '2'),
(4, 'qweqwe', 'ads', 'asd', 'qwe', 'asd'),
(8, '123123123', '23213', '4d4eac90e462b0ad31276ead8fa21620', '23213', '2'),
(9, '123', 'qwe', '4d4eac90e462b0ad31276ead8fa21620', 'qwe', 'q'),
(10, '123', '123', 'a0d5c8a4d386f15284ec25fe1eeeb426', '123_123', '123'),
(11, 'asd', 'asd', '5cebd3c130b8a8ab31fa2efb6a1ae8a7', 'asd_asd', 'asd'),
(12, '1', 'jose', '4d4eac90e462b0ad31276ead8fa21620', '1_jose', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block_web`
--
ALTER TABLE `block_web`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estacion`
--
ALTER TABLE `estacion`
 ADD PRIMARY KEY (`id`), ADD KEY `id_laboratorio` (`id_laboratorio`);

--
-- Indexes for table `historial_usuario`
--
ALTER TABLE `historial_usuario`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laboratorio`
--
ALTER TABLE `laboratorio`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `block_web`
--
ALTER TABLE `block_web`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `estacion`
--
ALTER TABLE `estacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `historial_usuario`
--
ALTER TABLE `historial_usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `laboratorio`
--
ALTER TABLE `laboratorio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `estacion`
--
ALTER TABLE `estacion`
ADD CONSTRAINT `estacion_ibfk_1` FOREIGN KEY (`id_laboratorio`) REFERENCES `laboratorio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
