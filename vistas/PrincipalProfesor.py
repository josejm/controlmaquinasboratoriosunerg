# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PrincipalProfesor.ui'
#
# Created: Sun May 14 20:02:05 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(745, 509)
        MainWindow.setMinimumSize(QtCore.QSize(650, 0))
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(270, 10, 171, 16))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        font.setKerning(False)
        self.label.setFont(font)
        self.label.setOpenExternalLinks(False)
        self.label.setObjectName(_fromUtf8("label"))
        self.maquinas = QtGui.QTableWidget(self.centralwidget)
        self.maquinas.setGeometry(QtCore.QRect(0, 30, 741, 401))
        self.maquinas.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.maquinas.setDragDropOverwriteMode(False)
        self.maquinas.setAlternatingRowColors(True)
        self.maquinas.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.maquinas.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.maquinas.setColumnCount(5)
        self.maquinas.setObjectName(_fromUtf8("maquinas"))
        self.maquinas.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.maquinas.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.maquinas.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.maquinas.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.maquinas.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.maquinas.setHorizontalHeaderItem(4, item)
        self.maquinas.horizontalHeader().setDefaultSectionSize(133)
        self.maquinas.horizontalHeader().setMinimumSectionSize(50)
        self.maquinas.verticalHeader().setDefaultSectionSize(50)
        self.maquinas.verticalHeader().setMinimumSectionSize(40)
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(0, 480, 744, 29))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.btnInvitado = QtGui.QPushButton(self.layoutWidget)
        self.btnInvitado.setObjectName(_fromUtf8("btnInvitado"))
        self.horizontalLayout.addWidget(self.btnInvitado)
        self.ApagarMaquinas = QtGui.QPushButton(self.layoutWidget)
        self.ApagarMaquinas.setObjectName(_fromUtf8("ApagarMaquinas"))
        self.horizontalLayout.addWidget(self.ApagarMaquinas)
        self.btnInternet = QtGui.QPushButton(self.layoutWidget)
        self.btnInternet.setObjectName(_fromUtf8("btnInternet"))
        self.horizontalLayout.addWidget(self.btnInternet)
        self.AgregarUsuario = QtGui.QPushButton(self.layoutWidget)
        self.AgregarUsuario.setObjectName(_fromUtf8("AgregarUsuario"))
        self.horizontalLayout.addWidget(self.AgregarUsuario)
        self.layoutWidget1 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget1.setGeometry(QtCore.QRect(210, 440, 276, 29))
        self.layoutWidget1.setObjectName(_fromUtf8("layoutWidget1"))
        self.gridLayout = QtGui.QGridLayout(self.layoutWidget1)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.Apagar = QtGui.QPushButton(self.layoutWidget1)
        self.Apagar.setObjectName(_fromUtf8("Apagar"))
        self.gridLayout.addWidget(self.Apagar, 0, 0, 1, 1)
        self.Actualizar = QtGui.QPushButton(self.layoutWidget1)
        self.Actualizar.setObjectName(_fromUtf8("Actualizar"))
        self.gridLayout.addWidget(self.Actualizar, 0, 1, 1, 1)
        self.btnVerHistorial = QtGui.QPushButton(self.layoutWidget1)
        self.btnVerHistorial.setObjectName(_fromUtf8("btnVerHistorial"))
        self.gridLayout.addWidget(self.btnVerHistorial, 0, 2, 1, 1)
        self.lblInternet = QtGui.QLabel(self.centralwidget)
        self.lblInternet.setGeometry(QtCore.QRect(500, 450, 151, 16))
        self.lblInternet.setText(_fromUtf8(""))
        self.lblInternet.setObjectName(_fromUtf8("lblInternet"))
        self.lblInvitado = QtGui.QLabel(self.centralwidget)
        self.lblInvitado.setGeometry(QtCore.QRect(10, 440, 181, 20))
        self.lblInvitado.setText(_fromUtf8(""))
        self.lblInvitado.setObjectName(_fromUtf8("lblInvitado"))
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", ".::PROFESOR::.", None))
        self.label.setText(_translate("MainWindow", "Maquinas del laboratorio", None))
        item = self.maquinas.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Numero", None))
        item = self.maquinas.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Ip", None))
        item = self.maquinas.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Conectado", None))
        item = self.maquinas.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Fecha/Hora", None))
        item = self.maquinas.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Tiempo Encendido", None))
        self.btnInvitado.setText(_translate("MainWindow", "Activar/Desactivar Sesion de Invitado", None))
        self.ApagarMaquinas.setText(_translate("MainWindow", "Apagar Todas Las Maquinas", None))
        self.btnInternet.setText(_translate("MainWindow", "Actvar/Desactivar Internet", None))
        self.AgregarUsuario.setText(_translate("MainWindow", "Agregar Usuario", None))
        self.Apagar.setText(_translate("MainWindow", "Apagar", None))
        self.Actualizar.setText(_translate("MainWindow", "Actualizar", None))
        self.btnVerHistorial.setText(_translate("MainWindow", "Ver Historial", None))

