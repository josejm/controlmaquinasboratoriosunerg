# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NuevoUsuario.ui'
#
# Created: Sun May 14 20:02:05 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_NuevoUsuario(object):
    def setupUi(self, NuevoUsuario):
        NuevoUsuario.setObjectName(_fromUtf8("NuevoUsuario"))
        NuevoUsuario.resize(341, 184)
        self.centralwidget = QtGui.QWidget(NuevoUsuario)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.lblMensaje = QtGui.QLabel(self.centralwidget)
        self.lblMensaje.setGeometry(QtCore.QRect(150, 0, 16, 16))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(0, 0, 341, 141))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.layoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label_4 = QtGui.QLabel(self.layoutWidget)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_4)
        self.txtSeccion = QtGui.QLineEdit(self.layoutWidget)
        self.txtSeccion.setObjectName(_fromUtf8("txtSeccion"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.txtSeccion)
        self.label = QtGui.QLabel(self.layoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label)
        self.txtUser = QtGui.QLineEdit(self.layoutWidget)
        self.txtUser.setObjectName(_fromUtf8("txtUser"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.txtUser)
        self.label_2 = QtGui.QLabel(self.layoutWidget)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_2)
        self.txtPassword = QtGui.QLineEdit(self.layoutWidget)
        self.txtPassword.setEchoMode(QtGui.QLineEdit.Password)
        self.txtPassword.setObjectName(_fromUtf8("txtPassword"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.txtPassword)
        self.label_3 = QtGui.QLabel(self.layoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_3)
        self.txtCedula = QtGui.QLineEdit(self.layoutWidget)
        self.txtCedula.setObjectName(_fromUtf8("txtCedula"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.txtCedula)
        self.layoutWidget1 = QtGui.QWidget(self.centralwidget)
        self.layoutWidget1.setGeometry(QtCore.QRect(80, 150, 178, 29))
        self.layoutWidget1.setObjectName(_fromUtf8("layoutWidget1"))
        self.formLayout_2 = QtGui.QFormLayout(self.layoutWidget1)
        self.formLayout_2.setMargin(0)
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.btnCacelar = QtGui.QPushButton(self.layoutWidget1)
        self.btnCacelar.setObjectName(_fromUtf8("btnCacelar"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.LabelRole, self.btnCacelar)
        self.btnGuardar = QtGui.QPushButton(self.layoutWidget1)
        self.btnGuardar.setObjectName(_fromUtf8("btnGuardar"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.FieldRole, self.btnGuardar)
        NuevoUsuario.setCentralWidget(self.centralwidget)

        self.retranslateUi(NuevoUsuario)
        QtCore.QMetaObject.connectSlotsByName(NuevoUsuario)

    def retranslateUi(self, NuevoUsuario):
        NuevoUsuario.setWindowTitle(_translate("NuevoUsuario", ".::NUEVO USUARIO::.", None))
        self.label_4.setText(_translate("NuevoUsuario", "Seccion:", None))
        self.label.setText(_translate("NuevoUsuario", "User:", None))
        self.label_2.setText(_translate("NuevoUsuario", "Password:", None))
        self.label_3.setText(_translate("NuevoUsuario", "Cedula:", None))
        self.btnCacelar.setText(_translate("NuevoUsuario", "Cancelar", None))
        self.btnGuardar.setText(_translate("NuevoUsuario", "Guardar", None))

