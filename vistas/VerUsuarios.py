# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VerUsuarios.ui'
#
# Created: Sun May 14 20:02:06 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_VerUsuarios(object):
    def setupUi(self, VerUsuarios):
        VerUsuarios.setObjectName(_fromUtf8("VerUsuarios"))
        VerUsuarios.resize(514, 411)
        self.layoutWidget = QtGui.QWidget(VerUsuarios)
        self.layoutWidget.setGeometry(QtCore.QRect(9, 9, 496, 393))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.splitter = QtGui.QSplitter(self.layoutWidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.label = QtGui.QLabel(self.splitter)
        self.label.setObjectName(_fromUtf8("label"))
        self.comboSeccion = QtGui.QComboBox(self.splitter)
        self.comboSeccion.setObjectName(_fromUtf8("comboSeccion"))
        self.comboSeccion.addItem(_fromUtf8(""))
        self.btnBuscar = QtGui.QPushButton(self.splitter)
        self.btnBuscar.setObjectName(_fromUtf8("btnBuscar"))
        self.verticalLayout.addWidget(self.splitter)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.UsuariosTable = QtGui.QTableWidget(self.layoutWidget)
        self.UsuariosTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.UsuariosTable.setDragDropOverwriteMode(False)
        self.UsuariosTable.setAlternatingRowColors(True)
        self.UsuariosTable.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.UsuariosTable.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.UsuariosTable.setObjectName(_fromUtf8("UsuariosTable"))
        self.UsuariosTable.setColumnCount(4)
        self.UsuariosTable.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.UsuariosTable.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.UsuariosTable.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.UsuariosTable.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.UsuariosTable.setHorizontalHeaderItem(3, item)
        self.verticalLayout_3.addWidget(self.UsuariosTable)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.btnAgregar = QtGui.QPushButton(self.layoutWidget)
        self.btnAgregar.setObjectName(_fromUtf8("btnAgregar"))
        self.verticalLayout_2.addWidget(self.btnAgregar)
        self.btnEditar = QtGui.QPushButton(self.layoutWidget)
        self.btnEditar.setObjectName(_fromUtf8("btnEditar"))
        self.verticalLayout_2.addWidget(self.btnEditar)
        self.btnEliminar = QtGui.QPushButton(self.layoutWidget)
        self.btnEliminar.setObjectName(_fromUtf8("btnEliminar"))
        self.verticalLayout_2.addWidget(self.btnEliminar)
        self.btnVerHistorial = QtGui.QPushButton(self.layoutWidget)
        self.btnVerHistorial.setObjectName(_fromUtf8("btnVerHistorial"))
        self.verticalLayout_2.addWidget(self.btnVerHistorial)
        self.btnEliminarCarpetaCompartida = QtGui.QPushButton(self.layoutWidget)
        self.btnEliminarCarpetaCompartida.setObjectName(_fromUtf8("btnEliminarCarpetaCompartida"))
        self.verticalLayout_2.addWidget(self.btnEliminarCarpetaCompartida)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)
        self.verticalLayout.addLayout(self.verticalLayout_3)

        self.retranslateUi(VerUsuarios)
        QtCore.QMetaObject.connectSlotsByName(VerUsuarios)

    def retranslateUi(self, VerUsuarios):
        VerUsuarios.setWindowTitle(_translate("VerUsuarios", ".::VER USUARIOS::.", None))
        self.label.setText(_translate("VerUsuarios", "Seccion:", None))
        self.comboSeccion.setItemText(0, _translate("VerUsuarios", "Todos", None))
        self.btnBuscar.setText(_translate("VerUsuarios", "Buscar", None))
        item = self.UsuariosTable.horizontalHeaderItem(0)
        item.setText(_translate("VerUsuarios", "Seccion", None))
        item = self.UsuariosTable.horizontalHeaderItem(1)
        item.setText(_translate("VerUsuarios", "Cedula", None))
        item = self.UsuariosTable.horizontalHeaderItem(2)
        item.setText(_translate("VerUsuarios", "Usuario", None))
        item = self.UsuariosTable.horizontalHeaderItem(3)
        item.setText(_translate("VerUsuarios", "Carpeta", None))
        self.btnAgregar.setText(_translate("VerUsuarios", "Agregar", None))
        self.btnEditar.setText(_translate("VerUsuarios", "Editar", None))
        self.btnEliminar.setText(_translate("VerUsuarios", "Eliminar", None))
        self.btnVerHistorial.setText(_translate("VerUsuarios", "Ver HIstorial", None))
        self.btnEliminarCarpetaCompartida.setText(_translate("VerUsuarios", "Eliminar carpeta compartida", None))

