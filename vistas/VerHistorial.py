# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VerHistorial.ui'
#
# Created: Sun May 14 20:02:06 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_VerHistorial(object):
    def setupUi(self, VerHistorial):
        VerHistorial.setObjectName(_fromUtf8("VerHistorial"))
        VerHistorial.resize(632, 384)
        self.TableHistoriales = QtGui.QTableWidget(VerHistorial)
        self.TableHistoriales.setGeometry(QtCore.QRect(-1, -1, 631, 381))
        self.TableHistoriales.setAutoFillBackground(True)
        self.TableHistoriales.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.TableHistoriales.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.TableHistoriales.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.TableHistoriales.setObjectName(_fromUtf8("TableHistoriales"))
        self.TableHistoriales.setColumnCount(5)
        self.TableHistoriales.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.TableHistoriales.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.TableHistoriales.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.TableHistoriales.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.TableHistoriales.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.TableHistoriales.setHorizontalHeaderItem(4, item)

        self.retranslateUi(VerHistorial)
        QtCore.QMetaObject.connectSlotsByName(VerHistorial)

    def retranslateUi(self, VerHistorial):
        VerHistorial.setWindowTitle(_translate("VerHistorial", ".::VER HISTORIAL::.", None))
        item = self.TableHistoriales.horizontalHeaderItem(0)
        item.setText(_translate("VerHistorial", "Usuario", None))
        item = self.TableHistoriales.horizontalHeaderItem(1)
        item.setText(_translate("VerHistorial", "Laboratorio", None))
        item = self.TableHistoriales.horizontalHeaderItem(2)
        item.setText(_translate("VerHistorial", "Maquina", None))
        item = self.TableHistoriales.horizontalHeaderItem(3)
        item.setText(_translate("VerHistorial", "Accion", None))
        item = self.TableHistoriales.horizontalHeaderItem(4)
        item.setText(_translate("VerHistorial", "Fecha", None))

