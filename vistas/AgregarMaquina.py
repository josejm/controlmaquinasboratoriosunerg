# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AgregarMaquina.ui'
#
# Created: Sun May 14 20:02:05 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_AgregarMaquina(object):
    def setupUi(self, AgregarMaquina):
        AgregarMaquina.setObjectName(_fromUtf8("AgregarMaquina"))
        AgregarMaquina.resize(291, 227)
        self.lblMensaje = QtGui.QLabel(AgregarMaquina)
        self.lblMensaje.setGeometry(QtCore.QRect(9, 9, 16, 16))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))
        self.layoutWidget = QtGui.QWidget(AgregarMaquina)
        self.layoutWidget.setGeometry(QtCore.QRect(50, 180, 186, 37))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.layoutWidget)
        self.formLayout.setMargin(0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.btnCancelar = QtGui.QPushButton(self.layoutWidget)
        self.btnCancelar.setObjectName(_fromUtf8("btnCancelar"))
        self.horizontalLayout.addWidget(self.btnCancelar)
        self.btnGuardar = QtGui.QPushButton(self.layoutWidget)
        self.btnGuardar.setObjectName(_fromUtf8("btnGuardar"))
        self.horizontalLayout.addWidget(self.btnGuardar)
        self.formLayout.setLayout(0, QtGui.QFormLayout.FieldRole, self.horizontalLayout)
        self.widget = QtGui.QWidget(AgregarMaquina)
        self.widget.setGeometry(QtCore.QRect(-1, 4, 291, 171))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.gridLayout = QtGui.QGridLayout(self.widget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_5 = QtGui.QLabel(self.widget)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 0, 0, 1, 1)
        self.label = QtGui.QLabel(self.widget)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.txtPassword = QtGui.QLineEdit(self.widget)
        self.txtPassword.setObjectName(_fromUtf8("txtPassword"))
        self.gridLayout.addWidget(self.txtPassword, 5, 1, 1, 1)
        self.label_3 = QtGui.QLabel(self.widget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 5, 0, 1, 1)
        self.label_2 = QtGui.QLabel(self.widget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 4, 0, 1, 1)
        self.txtIP = QtGui.QLineEdit(self.widget)
        self.txtIP.setObjectName(_fromUtf8("txtIP"))
        self.gridLayout.addWidget(self.txtIP, 3, 1, 1, 1)
        self.comboLaboratorio = QtGui.QComboBox(self.widget)
        self.comboLaboratorio.setObjectName(_fromUtf8("comboLaboratorio"))
        self.gridLayout.addWidget(self.comboLaboratorio, 0, 1, 1, 1)
        self.txtNumero = QtGui.QLineEdit(self.widget)
        self.txtNumero.setText(_fromUtf8(""))
        self.txtNumero.setObjectName(_fromUtf8("txtNumero"))
        self.gridLayout.addWidget(self.txtNumero, 1, 1, 1, 1)
        self.txtUser = QtGui.QLineEdit(self.widget)
        self.txtUser.setObjectName(_fromUtf8("txtUser"))
        self.gridLayout.addWidget(self.txtUser, 4, 1, 1, 1)
        self.label_4 = QtGui.QLabel(self.widget)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)

        self.retranslateUi(AgregarMaquina)
        QtCore.QMetaObject.connectSlotsByName(AgregarMaquina)

    def retranslateUi(self, AgregarMaquina):
        AgregarMaquina.setWindowTitle(_translate("AgregarMaquina", ".::AGREGAR MAQUINA::.", None))
        self.btnCancelar.setText(_translate("AgregarMaquina", "Cancelar", None))
        self.btnGuardar.setText(_translate("AgregarMaquina", "Guardar", None))
        self.label_5.setText(_translate("AgregarMaquina", "Laboratorio:", None))
        self.label.setText(_translate("AgregarMaquina", "Numero:", None))
        self.label_3.setText(_translate("AgregarMaquina", "Password:", None))
        self.label_2.setText(_translate("AgregarMaquina", "User:", None))
        self.label_4.setText(_translate("AgregarMaquina", "Ip:", None))

