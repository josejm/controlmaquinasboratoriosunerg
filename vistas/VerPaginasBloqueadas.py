# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VerPaginasBloqueadas.ui'
#
# Created: Sun May 14 20:02:06 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_BloquearPaginas(object):
    def setupUi(self, BloquearPaginas):
        BloquearPaginas.setObjectName(_fromUtf8("BloquearPaginas"))
        BloquearPaginas.resize(293, 304)
        self.centralwidget = QtGui.QWidget(BloquearPaginas)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.lblMensaje = QtGui.QLabel(self.centralwidget)
        self.lblMensaje.setGeometry(QtCore.QRect(10, 10, 271, 16))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 30, 291, 29))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.txtPagina = QtGui.QLineEdit(self.widget)
        self.txtPagina.setObjectName(_fromUtf8("txtPagina"))
        self.horizontalLayout.addWidget(self.txtPagina)
        self.btnAgregar = QtGui.QPushButton(self.widget)
        self.btnAgregar.setObjectName(_fromUtf8("btnAgregar"))
        self.horizontalLayout.addWidget(self.btnAgregar)
        self.widget1 = QtGui.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(0, 60, 291, 241))
        self.widget1.setObjectName(_fromUtf8("widget1"))
        self.gridLayout = QtGui.QGridLayout(self.widget1)
        self.gridLayout.setMargin(0)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.Paginas = QtGui.QTableWidget(self.widget1)
        self.Paginas.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.Paginas.setAlternatingRowColors(True)
        self.Paginas.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.Paginas.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.Paginas.setObjectName(_fromUtf8("Paginas"))
        self.Paginas.setColumnCount(1)
        self.Paginas.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.Paginas.setHorizontalHeaderItem(0, item)
        self.gridLayout.addWidget(self.Paginas, 0, 0, 1, 1)
        self.btnEliminar = QtGui.QPushButton(self.widget1)
        self.btnEliminar.setObjectName(_fromUtf8("btnEliminar"))
        self.gridLayout.addWidget(self.btnEliminar, 1, 0, 1, 1)
        BloquearPaginas.setCentralWidget(self.centralwidget)

        self.retranslateUi(BloquearPaginas)
        QtCore.QMetaObject.connectSlotsByName(BloquearPaginas)

    def retranslateUi(self, BloquearPaginas):
        BloquearPaginas.setWindowTitle(_translate("BloquearPaginas", ".::VER ṔAGINAS BLOQUEADAS::.", None))
        self.btnAgregar.setText(_translate("BloquearPaginas", "Agregar", None))
        item = self.Paginas.horizontalHeaderItem(0)
        item.setText(_translate("BloquearPaginas", "Pagina", None))
        self.btnEliminar.setText(_translate("BloquearPaginas", "Eliminar", None))

