# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'NuevaEstacion.ui'
#
# Created: Sun May 14 20:02:05 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(301, 207)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(-1, 7, 301, 201))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_3 = QtGui.QLabel(self.layoutWidget)
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.comboLab = QtGui.QComboBox(self.layoutWidget)
        self.comboLab.setObjectName(_fromUtf8("comboLab"))
        self.gridLayout.addWidget(self.comboLab, 0, 1, 1, 1)
        self.label = QtGui.QLabel(self.layoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName(_fromUtf8("label"))
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.txtNumero = QtGui.QLineEdit(self.layoutWidget)
        self.txtNumero.setObjectName(_fromUtf8("txtNumero"))
        self.gridLayout.addWidget(self.txtNumero, 1, 1, 1, 1)
        self.label_2 = QtGui.QLabel(self.layoutWidget)
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)
        self.txtIp = QtGui.QLineEdit(self.layoutWidget)
        self.txtIp.setObjectName(_fromUtf8("txtIp"))
        self.gridLayout.addWidget(self.txtIp, 2, 1, 1, 1)
        self.label_4 = QtGui.QLabel(self.layoutWidget)
        self.label_4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.txtUsername = QtGui.QLineEdit(self.layoutWidget)
        self.txtUsername.setObjectName(_fromUtf8("txtUsername"))
        self.gridLayout.addWidget(self.txtUsername, 3, 1, 1, 1)
        self.label_5 = QtGui.QLabel(self.layoutWidget)
        self.label_5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.gridLayout.addWidget(self.label_5, 4, 0, 1, 1)
        self.txtPassword = QtGui.QLineEdit(self.layoutWidget)
        self.txtPassword.setObjectName(_fromUtf8("txtPassword"))
        self.gridLayout.addWidget(self.txtPassword, 4, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.btnCancelar = QtGui.QPushButton(self.layoutWidget)
        self.btnCancelar.setObjectName(_fromUtf8("btnCancelar"))
        self.gridLayout_2.addWidget(self.btnCancelar, 0, 0, 1, 1)
        self.btnGuardar = QtGui.QPushButton(self.layoutWidget)
        self.btnGuardar.setObjectName(_fromUtf8("btnGuardar"))
        self.gridLayout_2.addWidget(self.btnGuardar, 0, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", ".::NUEVA ESTACION::.", None))
        self.label_3.setText(_translate("MainWindow", "Laboratorios:", None))
        self.label.setText(_translate("MainWindow", "Numero:", None))
        self.label_2.setText(_translate("MainWindow", "IP", None))
        self.label_4.setText(_translate("MainWindow", "Username", None))
        self.label_5.setText(_translate("MainWindow", "Password", None))
        self.btnCancelar.setText(_translate("MainWindow", "Cancelar", None))
        self.btnGuardar.setText(_translate("MainWindow", "Guardar", None))

