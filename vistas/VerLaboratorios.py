# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VerLaboratorios.ui'
#
# Created: Sun May 14 20:02:06 2017
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Laboratorios(object):
    def setupUi(self, Laboratorios):
        Laboratorios.setObjectName(_fromUtf8("Laboratorios"))
        Laboratorios.resize(260, 333)
        self.lblMensaje = QtGui.QLabel(Laboratorios)
        self.lblMensaje.setGeometry(QtCore.QRect(133, 170, 16, 16))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))
        self.layoutWidget = QtGui.QWidget(Laboratorios)
        self.layoutWidget.setGeometry(QtCore.QRect(9, 9, 242, 315))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.layoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.txtLaboratorio = QtGui.QLineEdit(self.layoutWidget)
        self.txtLaboratorio.setObjectName(_fromUtf8("txtLaboratorio"))
        self.horizontalLayout.addWidget(self.txtLaboratorio)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.btnAgregar = QtGui.QPushButton(self.layoutWidget)
        self.btnAgregar.setObjectName(_fromUtf8("btnAgregar"))
        self.verticalLayout.addWidget(self.btnAgregar)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        self.LaboratoriosTable = QtGui.QTableWidget(self.layoutWidget)
        self.LaboratoriosTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.LaboratoriosTable.setDragDropOverwriteMode(False)
        self.LaboratoriosTable.setAlternatingRowColors(True)
        self.LaboratoriosTable.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.LaboratoriosTable.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.LaboratoriosTable.setObjectName(_fromUtf8("LaboratoriosTable"))
        self.LaboratoriosTable.setColumnCount(2)
        self.LaboratoriosTable.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.LaboratoriosTable.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.LaboratoriosTable.setHorizontalHeaderItem(1, item)
        self.verticalLayout_3.addWidget(self.LaboratoriosTable)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.btnEliminar = QtGui.QPushButton(self.layoutWidget)
        self.btnEliminar.setObjectName(_fromUtf8("btnEliminar"))
        self.verticalLayout_2.addWidget(self.btnEliminar)
        self.btnInternet = QtGui.QPushButton(self.layoutWidget)
        self.btnInternet.setObjectName(_fromUtf8("btnInternet"))
        self.verticalLayout_2.addWidget(self.btnInternet)
        self.btnVerHistorial = QtGui.QPushButton(self.layoutWidget)
        self.btnVerHistorial.setObjectName(_fromUtf8("btnVerHistorial"))
        self.verticalLayout_2.addWidget(self.btnVerHistorial)
        self.btnEliminarCarpetasCompartidas = QtGui.QPushButton(self.layoutWidget)
        self.btnEliminarCarpetasCompartidas.setObjectName(_fromUtf8("btnEliminarCarpetasCompartidas"))
        self.verticalLayout_2.addWidget(self.btnEliminarCarpetasCompartidas)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)

        self.retranslateUi(Laboratorios)
        QtCore.QMetaObject.connectSlotsByName(Laboratorios)

    def retranslateUi(self, Laboratorios):
        Laboratorios.setWindowTitle(_translate("Laboratorios", ".::VER LABORATORIOS::.", None))
        self.label.setText(_translate("Laboratorios", "Laboratorio:", None))
        self.btnAgregar.setText(_translate("Laboratorios", "Agregar", None))
        item = self.LaboratoriosTable.horizontalHeaderItem(0)
        item.setText(_translate("Laboratorios", "                                   Laboratorio                                 ", None))
        item = self.LaboratoriosTable.horizontalHeaderItem(1)
        item.setText(_translate("Laboratorios", "                                   Internet                                    ", None))
        self.btnEliminar.setText(_translate("Laboratorios", "Eliminar", None))
        self.btnInternet.setText(_translate("Laboratorios", "Activar/Desactivar Internet", None))
        self.btnVerHistorial.setText(_translate("Laboratorios", "Ver Historial", None))
        self.btnEliminarCarpetasCompartidas.setText(_translate("Laboratorios", "Eliminar Carpetas Compartidas", None))

