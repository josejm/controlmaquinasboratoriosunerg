from PyQt4 import QtGui

import sys
from vistas import VerLaboratorios
from core.historial import *
from core.estacion import estacion
from model.model import model
from core.laboratorio import laboratorios
from controlador import VerHistorialController

class VerLaboratoriosController(QtGui.QMainWindow,QtGui.QDialog, VerLaboratorios.Ui_Laboratorios):

  labs = None
  estacion=None
  bd= model()
  def __init__(self, parent=None):
    super(VerLaboratoriosController, self).__init__(parent)
    self.setupUi(self)
    self.btnAgregar.clicked.connect(self.AgregarLaboratorio)
    self.btnEliminar.clicked.connect(self.EliminarLaboratorio)
    self.btnInternet.clicked.connect(self.ActivarDesactivarInternet)
    self.btnVerHistorial.clicked.connect(self.VerHistorial)

  def AgregarLaboratorio(self):
    if(self.txtLaboratorio.text()==""):
      self.lblMensaje.setText("Ingrese un numero de laboratorio!")
    else:
      self.lblMensaje.setText("")
      self.bd.InsertLaboratorio(self.txtLaboratorio.text(),1)
      self.CargarData()

  def EliminarLaboratorio(self):
    numero= self.LaboratoriosTable.item(self.LaboratoriosTable.currentRow(), 0).text()
    for lab in self.labs.laboratorios:
      if str(lab.numero)==str(numero):
        self.bd.DeleteEstacionesPorIdLaboratorio(lab.id_laboratorio)
        self.bd.DeleteLaboratorioPorId(lab.id_laboratorio)
        break
    self.CargarData()

  def ActivarDesactivarInternet(self):
    numero= self.LaboratoriosTable.item(self.LaboratoriosTable.currentRow(), 0).text()
    for lab in self.labs.laboratorios:
      if str(lab.numero)==str(numero):
        if lab.internet==1:
           lab.DesactivarInternet()
        else:
           lab.ActivarInternet()
        break
    self.CargarData()

  def VerHistorial(self):
    laboratorio= self.LaboratoriosTable.item(self.LaboratoriosTable.currentRow(), 0).text()
    h=historiales()
    h.BuscarPorLaboratorio(laboratorio)
    VerHistorialController.VerHistorialController(self).main(h)

  def main(self):
    self.show()
    self.CargarData()

  def CargarData(self):
    self.labs=laboratorios()
    row=0
    self.LaboratoriosTable.setRowCount(0)
    for lab in self.labs.laboratorios:
      self.LaboratoriosTable.insertRow(row)
      laboratorio = QtGui.QTableWidgetItem(str(lab.numero))
      internet = QtGui.QTableWidgetItem(str(lab.internet))
      self.LaboratoriosTable.setItem(row,0, laboratorio)
      self.LaboratoriosTable.setItem(row,1, internet)
      row+=1


