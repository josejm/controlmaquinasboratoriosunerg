from PyQt4 import QtGui

import sys
from vistas import NuevoUsuario
from core.usuario import usuario



class NuevoUsuarioController(QtGui.QMainWindow,QtGui.QDialog, NuevoUsuario.Ui_NuevoUsuario):

  user= usuario()


  def __init__(self, parent=None):

    super(NuevoUsuarioController, self).__init__(parent)
    self.setupUi(self)
    self.btnCacelar.clicked.connect(self.Cancelar)
    self.btnGuardar.clicked.connect(self.Guardar)

  def Cancelar(self):
    self.close()
  
  def Guardar(self):
    if(self.txtSeccion.text()==""):
      self.lblMensaje.setText("Ingrese una seccion!")
    if(self.txtCedula.text()==""):
      self.lblMensaje.setText("Ingrese la cedula!")
    elif(self.txtUser.text()==""):
      self.lblMensaje.setText("Ingrese un usuario!")
    elif(self.txtPassword.text()==""):
      self.lblMensaje.setText("Ingrese un password!")
    else:
      self.lblMensaje.setText("Usuario Guardado")
      if self.user.user=="":
        self.user.InsertarUsuario(self.txtUser.text(),self.txtCedula.text(),self.txtPassword.text(),self.txtSeccion.text())
      else:
        self.user.user=self.txtUser.text()
        self.user.seccion=self.txtSeccion.text()
        self.user.cedula=self.txtCedula.text()
        self.user.password=self.txtPassword.text()
        self.user.ActualizarUsuario()
      self.close()

  def main(self,user=None):
    self.show()
    if user!=None:
      self.user.BuscarUsuarioPorUser(user)
      self.txtSeccion.setText(str(self.user.seccion))
      self.txtCedula.setText(str(self.user.cedula))
      self.txtUser.setText(str(self.user.user))
      self.txtPassword.setText(str(self.user.password))
    else:
      self.user= usuario()


