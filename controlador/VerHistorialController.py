from PyQt4 import QtGui

import sys
from vistas import VerHistorial
from core.estacion import estacion
from model.model import model
from core.laboratorio import laboratorios


class VerHistorialController(QtGui.QMainWindow,QtGui.QDialog, VerHistorial.Ui_VerHistorial):


  historiales=None
  bd= model()
  def __init__(self, parent=None):
    super(VerHistorialController, self).__init__(parent)
    self.setupUi(self)

  def main(self,historiales):
    self.show()
    self.historiales=historiales
    self.CargarData()

  def CargarData(self):
    self.TableHistoriales.setRowCount(0)
    row=0
    for h in self.historiales.historiales:
          self.TableHistoriales.insertRow(row)
          usuario = QtGui.QTableWidgetItem(str(h.usuario))
          laboratorio = QtGui.QTableWidgetItem(str(h.laboratorio))
          maquina = QtGui.QTableWidgetItem(str(h.maquina))
          accion = QtGui.QTableWidgetItem(str(h.accion))
          fecha = QtGui.QTableWidgetItem(str(h.fecha))
          self.TableHistoriales.setItem(row,0, usuario)
          self.TableHistoriales.setItem(row,1, laboratorio)
          self.TableHistoriales.setItem(row,2, maquina)
          self.TableHistoriales.setItem(row,3, accion)
          self.TableHistoriales.setItem(row,4, fecha)
          row=row+1



