
from PyQt4 import QtGui

import sys
from vistas import AgregarMaquina
from core.estacion import estacion
from model.model import model
from core.laboratorio import laboratorios


class EditarAgregarMaquinaController(QtGui.QMainWindow,QtGui.QDialog, AgregarMaquina.Ui_AgregarMaquina):

  labs = None
  estacion=None
  bd= model()
  def __init__(self, parent=None):
    super(EditarAgregarMaquinaController, self).__init__(parent)
    self.setupUi(self)
    self.btnCancelar.clicked.connect(self.Cancelar)
    self.btnGuardar.clicked.connect(self.Guardar)

  def Cancelar(self):
    self.close()
  
  def Guardar(self):
    if(self.txtNumero.text()==""):
      self.lblMensaje.setText("Ingrese un numero!")
    elif(self.txtIP.text()==""):
      self.lblMensaje.setText("Ingrese una ip!")
    elif(self.txtPassword.text()==""):
      self.lblMensaje.setText("Ingrese un password!")
    else:
      self.lblMensaje.setText("Maquina Guardado")

      self.estacion.ip=self.txtIP.text()
      self.estacion.username=self.txtUser.text()
      self.estacion.password=self.txtPassword.text()
      lab=self.bd.GetLaboratorioPorNumero(self.comboLaboratorio.currentText())[0]
      self.estacion.id_laboratorio=str(lab[0])
      if(self.estacion.numero!=None):
        self.estacion.numero=self.txtNumero.text()
        self.estacion.EditarEstacion()
      else:
        self.estacion.numero=self.txtNumero.text()
        self.estacion.GuardarNuevaEstacion()
      self.close()

  def main(self,numero_maquina):
    self.show()
    self.labs=laboratorios()
    for lab in self.labs.laboratorios:
      self.comboLaboratorio.addItem(str(lab.numero))
    if(numero_maquina!=None):
      self.estacion=estacion(numero_maquina)
      self.txtNumero.setText(str(self.estacion.numero))
      self.txtIP.setText(self.estacion.ip)
      self.txtUser.setText(self.estacion.username)
      self.txtPassword.setText(self.estacion.password)
    else:
      self.estacion= estacion(None)


