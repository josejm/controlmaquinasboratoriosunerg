from PyQt4 import QtGui

import sys
from vistas import VerPaginasBloqueadas
from model.model import model



class VerPaginasBloqueadasController(QtGui.QMainWindow,QtGui.QDialog, VerPaginasBloqueadas.Ui_BloquearPaginas):

  bd= model()
  def __init__(self, parent=None):
    super(VerPaginasBloqueadasController, self).__init__(parent)
    self.setupUi(self)
    self.btnAgregar.clicked.connect(self.agregar)
    self.btnEliminar.clicked.connect(self.eliminar)

  def main(self):
    self.show()
    self.CargarData()

  def CargarData(self):
    webs=self.bd.GetPaginas()
    row=0
    self.Paginas.setRowCount(0)
    for p in webs:
      self.Paginas.insertRow(row)
      pagina = QtGui.QTableWidgetItem(str(p[1]))

      self.Paginas.setItem(row,0, pagina)
      row+=1

  def agregar(self):
   if self.txtPagina.text()=="":
      self.lblMensaje.setText("Ingrese una pagina!")
   else:
      self.bd.InsertPagina(self.txtPagina.text())
      self.CargarData()

  def eliminar(self):
    pagina= self.Paginas.item(self.Paginas.currentRow(), 0).text()
    self.bd.DeletePaginaPorWeb(pagina)
    self.CargarData()
