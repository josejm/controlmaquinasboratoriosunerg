from PyQt4 import QtGui

import sys
from vistas import VerUsuarios
from core.usuario import *
from model.model import model
from core.laboratorio import laboratorios
from controlador import NuevoUsuarioController,VerHistorialController
from core.historial import *

class VerUsuariosController(QtGui.QMainWindow,QtGui.QDialog, VerUsuarios.Ui_VerUsuarios):

  bd= model()
  usuarios=None

  def __init__(self, parent=None):
    super(VerUsuariosController, self).__init__(parent)
    self.setupUi(self)
    self.btnBuscar.clicked.connect(self.CargarData)
    self.btnEliminar.clicked.connect(self.EliminarUsuario)
    self.btnAgregar.clicked.connect(self.AgregarUsuario)
    self.btnEditar.clicked.connect(self.EditarUsuario)
    self.btnVerHistorial.clicked.connect(self.VerHistorial)

  def AgregarUsuario(self):
    NuevoUsuarioController.NuevoUsuarioController(self).main(None)

  def EditarUsuario(self):
    user= self.UsuariosTable.item(self.UsuariosTable.currentRow(), 2).text()
    NuevoUsuarioController.NuevoUsuarioController(self).main(user)

  def EliminarUsuario(self):
    user= self.UsuariosTable.item(self.UsuariosTable.currentRow(), 2).text()
    for us in self.usuarios.usuarios:
      if str(us.user)==str(user):
        print us.user
        us.EliminarUsuario()
        break
    self.CargarData()

  def VerHistorial(self):
    user= self.UsuariosTable.item(self.UsuariosTable.currentRow(), 2).text()
    h=historiales()
    h.BuscarPorUsuario(user)
    VerHistorialController.VerHistorialController(self).main(h)

  def ActivarDesactivarInternet(self):
    numero= self.LaboratoriosTable.item(self.LaboratoriosTable.currentRow(), 0).text()
    for lab in self.labs.laboratorios:
      if str(lab.numero)==str(numero):
        if lab.internet==1:
           lab.DesactivarInternet()
        else:
           lab.ActivarInternet()
        break
    self.CargarData()

  def main(self):
    self.show()
    self.CargarData()

  def CargarData(self):
    self.usuarios=usuarios()
    secciones=[]
    row=0
    self.UsuariosTable.setRowCount(0)
    for user in self.usuarios.usuarios:
      if self.comboSeccion.currentText()=="Todos" or self.comboSeccion.currentText()==str(user.seccion):
        self.UsuariosTable.insertRow(row)
        seccion = QtGui.QTableWidgetItem(str(user.seccion))
        cedula = QtGui.QTableWidgetItem(str(user.cedula))
        username = QtGui.QTableWidgetItem(str(user.user))
        carpeta = QtGui.QTableWidgetItem(str(user.carpeta))

        self.UsuariosTable.setItem(row,0, seccion)
        self.UsuariosTable.setItem(row,1, cedula)
        self.UsuariosTable.setItem(row,2, username)
        self.UsuariosTable.setItem(row,3, carpeta)
        row+=1
      secciones.append(user.seccion)

    index= self.comboSeccion.currentIndex()
    self.comboSeccion.clear()
    self.comboSeccion.addItem("Todos")
    for sec in secciones:
      self.comboSeccion.addItem(str(sec))
    self.comboSeccion.setCurrentIndex(index)


