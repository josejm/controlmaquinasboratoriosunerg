import conf
numero_lab= conf.numero_lab
from PyQt4 import QtGui
import sys

from core import laboratorio
from core.historial import *
from vistas import PrincipalProfesor
from controlador import NuevoUsuarioController,VerHistorialController
import threading
from core.ssh import ssh

class Principal(QtGui.QMainWindow, PrincipalProfesor.Ui_MainWindow):
 
  lab = laboratorio.lab(numero_lab)
  threads = None
  ssh= ssh()
  def __init__(self, parent=None):

    super(Principal, self).__init__(parent)
    self.setupUi(self)
    self.ApagarMaquinas.clicked.connect(self.lab.ApagarMaquinas)
    self.btnInternet.clicked.connect(self.ActivarDesactivarInternet)
    self.btnInvitado.clicked.connect(self.ActivarDesactivarInvitado)
    self.Actualizar.clicked.connect(self.ActualizarDataSubProceso)
    self.Apagar.clicked.connect(self.ApagarPc)
    self.AgregarUsuario.clicked.connect(self.NuevoUsuarioVentana)
    self.btnVerHistorial.clicked.connect(self.VerHistorial)
    self.ActualizarDataSubProceso()

  def ActualizarDataSubProceso(self):
    self.threads =threading.Thread(target=self.ActualizarData)
    self.threads.start();

  def ActualizarData(self):
    self.maquinas.setRowCount(0)
    row=0
    if self.lab.internet==1:
       self.lblInternet.setText("Internet Activa")
    else:
       self.lblInternet.setText("Internet Inactivada")

    if self.lab.invitado==1:
       self.lblInvitado.setText("Sesion de invitado Activa")
    else:
       self.lblInvitado.setText("Sesion de invitado Inactivada")
    for e in self.lab.estaciones:
      self.maquinas.insertRow(row)
      numero = QtGui.QTableWidgetItem(str(e.numero))
      ip = QtGui.QTableWidgetItem(str(e.ip))

      self.maquinas.setItem(row,0, numero)
      self.maquinas.setItem(row,1, ip)
      row=row+1

    row=0

    for e in self.lab.estaciones:

      e.Conectada()
      conecta = QtGui.QTableWidgetItem(str(e.maquina_conectada))
      self.maquinas.setItem(row,2, conecta)
     
      row=row+1

    row=0

    for e in self.lab.estaciones:

      e.Conectada()

      fechahora = QtGui.QTableWidgetItem(str(e.VerHoraYFechaUsuario()))
      tiempo = QtGui.QTableWidgetItem(str(e.VerTiempoEncendido()))


      self.maquinas.setItem(row,3, fechahora)
      self.maquinas.setItem(row,4, tiempo)
     
      row=row+1

  def VerHistorial(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 0).text()
    h=historiales()
    h.BuscarPorLaboratorioYMaquina(numero_lab,numero)
    VerHistorialController.VerHistorialController(self).main(h)

  def ApagarPc(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 0).text()
    print numero
    for e in self.lab.estaciones:
      if(str(e.numero)==numero):
        e.Apagar()
        break
    self.ActualizarDataSubProceso()

  def ActivarDesactivarInvitado(self): 
    if self.lab.invitado==1:
       self.lblInvitado.setText("Sesion de invitado Inactiva")
       self.lab.DesactivarInvitado()
    else:
       self.lblInvitado.setText("Sesion de invitado Activa")
       self.lab.ActivarInvitado()

  def ActivarDesactivarInternet(self):  
    if self.lab.internet==1:
       self.lblInternet.setText("Internet Inactiva")
       self.lab.DesactivarInternet()
    else:
       self.lblInternet.setText("Internet Activa")
       self.lab.ActivarInternet()
    self.ssh.GenerarListaIpsProxi(laboratorio.laboratorios())

  def NuevoUsuarioVentana(self):
    NuevoUsuarioController.NuevoUsuarioController(self).main()

def main():
  app = QtGui.QApplication(sys.argv)
  form = Principal()
  form.show()
  app.exec_()


if __name__ == '__main__':
  main()
