# libreria que permite la conexion ssh
import paramiko
import conf
from model.model import model

class ssh():
  ssh=None
  canal=None
  bd= model()
  #Constructor
  def __init__(self):
    self.ssh = paramiko.SSHClient()  # Iniciamos un cliente SSH
    self.ssh.load_system_host_keys()  # Agregamos el listado de host conocidos
    self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # Si no encuentra el host, lo agrega automaticamente

  # Se conecta a una computadora via ssh
  def Conectar(self,ip,Username,Password):
    try:
       # Conectamos al Servidro
       self.ssh = paramiko.Transport((ip, 22))
       self.ssh.connect(username = Username, password = Password)
       # Abrimos una sesion en el servidor
       self.canal = self.ssh.open_session()
       return True
    except paramiko.ssh_exception.AuthenticationException:
       return False

  # ejecuta un comando en una pc via ssh
  def EjecutarComando(self,comando,retornar=False):
    # si retornara la respueta del comando
    if(retornar):
      self.canal.exec_command(comando)
      return self.canal.makefile('rb', -1).readlines()
    # si no simplemente se ejecuta el comando
    else:
      self.canal.exec_command(comando)
      # Ejecutamos un comando

  # cierra la conexion ssh
  def CerrarConexion(self):
    self.ssh.close()

  # envia un comando de apagado (se permiso de super usuario en el pc remoto)
  def ApagarPc(self):
    self.EjecutarComando(conf.ssh_cmd_apagar)
    self.CerrarConexion()

  # verififica que usuario esta usando la pc mediante el comando WHO
  def VerUsuario(self):
     respuesta= self.EjecutarComando("who",True)[0].replace(' ',"")
     if(respuesta.find("0:")):
       return respuesta[0:respuesta.find(":")]
     else:
       return ""

  # crea una carpeta en una computadora
  def CrearCarpetaUsuario(self,patch_carpeta):
    self.EjecutarComando("mkdir "+patch_carpeta)
    self.CerrarConexion()

  # crea una carpeta en una computadora
  def BuscarCarpeta(self,patch_carpeta):
    respuesta= len(self.EjecutarComando("find "+patch_carpeta,True))>0
    self.CerrarConexion()
    return respuesta

  # verigica la fecha y hora en que el usuario ingreso con el comando WHO
  def VerHoraYFechaUsuario(self):
     respuesta= self.EjecutarComando("who",True)[0].replace(' ',"")
     return respuesta[respuesta.find(":0")::].replace(":0","")[:10]+" "+respuesta[respuesta.find(":0")::].replace(":0","")[10:]

  # verficica el tiempo que tiene la maquina encendido cone l comando uptime
  def VerTiempoEncendido(self):
     return self.EjecutarComando("uptime -p",True)[0].replace("\n","")

  def CrearCarpetaCompartidaServidor(self,username,name_directory,password):
     for cmd in conf.cmd_directory_share:
       self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
       cmd=cmd.replace("username",str(username))
       cmd=cmd.replace("password",str(password))
       cmd=cmd.replace("name_directory",str(name_directory))
       cmd=cmd.replace("username_name_directory",str(username)+"_"+str(name_directory))
       print cmd
       print self.EjecutarComando(cmd,True)
     self.CerrarConexion()

  def ReiniciarSamba(self):
     ## genera el archivo include
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando(conf.cmd_gen_files_samba)
    self.CerrarConexion()
    print "servidor reinciado"
    # reinicia el servidor samba
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando(conf.cmd_samba_reset)
    self.CerrarConexion()
    print "Include regenerado"

  def EliminarCarpetasCompartidas(self,usuarios):
    for user in usuarios.usuarios:
      self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
      print self.EjecutarComando("rm -r "+conf.patch_directory_share+"/"+user.carpeta+" "+conf.patch_samba_share_directory+"/"+str(user.user)+"_"+str(user.carpeta)+".conf.dir && "+conf.cmd_del_user.replace("username",str(user.user)),True)
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando(conf.cmd_gen_files_samba)
    self.CerrarConexion()

  def GenerarListaIpsProxi(self,labs):
    declaraciones=""
    reglas=""
    paginas_block=""
    paginas=self.bd.GetPaginas()
    nombre_grupo_pagina_bloqueadas=""
    for p in paginas:
       paginas_block+=p[1]+"\n"
    if(paginas_block):
      self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
      self.EjecutarComando("echo \""+str(paginas_block)+"\" > "+ conf.patch_proxi_paginas_block)
      self.CerrarConexion()

      nombre_grupo_pagina_bloqueadas="!webs_block"

    for lab in labs.laboratorios:
      # bucle para recorer las ips
      ips=""
      self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
      print "rm "+conf.directory_proxi_ips+"/"+conf.proxi_magic+str(lab.numero)
      self.EjecutarComando("rm "+conf.directory_proxi_ips+"/"+conf.proxi_magic+str(lab.numero))
      self.CerrarConexion()
      for estacion in lab.estaciones:
        ips+=estacion.ip+"\n"
      if(ips!=""):
        print str(lab.numero)+""+ips
        declaraciones+="acl "+conf.proxi_magic+str(lab.numero)+" src \""+ conf.directory_proxi_ips+"/"+conf.proxi_magic+str(lab.numero)+"\"\n"
        if lab.internet:
          reglas+="http_access allow "+nombre_grupo_pagina_bloqueadas+" "+conf.proxi_magic+str(lab.numero)+"\n"
        else:
          reglas+="http_access deny "+nombre_grupo_pagina_bloqueadas+" "+conf.proxi_magic+str(lab.numero)+"\n"
        self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
        self.EjecutarComando("echo \""+ips+"\" > "+ conf.directory_proxi_ips+"/"+conf.proxi_magic+str(lab.numero))
        self.CerrarConexion()
    if(paginas_block):
      declaraciones+="acl webs_block url_regex \""+conf.patch_proxi_paginas_block+"\"\n"
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando("echo \'"+declaraciones+"\' > "+ conf.patch_proxi_declaraciones_grupos)
    self.CerrarConexion()
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando("echo \'"+reglas+"\' > "+ conf.patch_proxi_reglas_grupos)
    self.CerrarConexion()
    self.Conectar(conf.IP_SERVER,conf.USER_SERVER,conf.PASSWORD_SERVER)
    self.EjecutarComando(conf.cmd_reconfigure)
    self.CerrarConexion()



