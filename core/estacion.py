# carga la clase model para interactuar con la base de datos squilte
from model.model import model
# carga la clase ssh
from core import ssh
# carga la funcion system para hacer ping
from os import system
import pexpect
import conf

# clase para una estacion
class estacion():
  # id de la estacion
  id_estacion=None
  # numero de la estacion
  numero=None
  # numero de la estacion
  numero_anterior=None
  # id del laboratorio
  id_laboratorio=None
  # id del laboratorio
  id_laboratorio_anterior=None
  # ip de la estacion
  ip=None
  # username de la estacion
  username=None
  # password de la estacion
  password=None
  # instancia para la clase model 
  db= model()
  # instancia para la clase ssh
  ssh=ssh.ssh()
  # maquina Conectada
  maquina_conectada=False

  # constructor
  def __init__(self,numero):
    if(numero!=None):
      # se busca la estacion
      estacion= self.db.GetEstacionesPorNumero(numero)[0]
      # se carga el id de la estacion
      self.id_estacion=estacion[0]
      # se carga el id de laboratorio
      self.id_laboratorio=estacion[1]
      # se carga el id de laboratorio
      self.id_laboratorio_anterior=estacion[1]
      # se carga el numero de la estacion
      self.numero=estacion[2]
      # se carga el numero de la estacion
      self.numero_anterior=estacion[2]
      # se carga la ip de la estacion
      self.ip=estacion[3]
      # se carga el usuario de la estacion
      self.username=estacion[4]
      # se carga el password de la estacion
      self.password=estacion[5]

  def BuscarEstacion(self):
    try:
      cmd = pexpect.spawn("sudo ifconfig | grep  Bcast > ip")
      cmd.expect(".") 
      cmd.sendline(str(conf.PASSWORD_ROOT))
      cmd.close()
      ifconfig= open("ip","r").readlines()[0].split(" ")
      ip=""
      for i in ifconfig:
         if( i.startswith("addr:") and len(i.split(":"))>1):
           ip=i.split(":")[1]
      if ip!="":
        estacion= self.db.GetEstacionesPorIP(ip)
        if len(estacion)>0:
          estacion=estacion[0]
          #se carga el id de la estacion
          self.id_estacion=estacion[0]
          #se carga el id de laboratorio
          self.id_laboratorio=estacion[1]
         # se carga el numero de la estacion
          self.numero=estacion[2]
          #se carga la ip de la estacion
          self.ip=estacion[3]
          #se carga el usuario de la estacion
          self.username=estacion[4]
          # se carga el password de la estacion
          self.password=estacion[5]
          return True
      return False
    except Exception:
      print "error con pexpect"

  # Actualiza los datos
  def ActualizarData(self):
    # se busca la estacion
    estacion= self.db.GetEstacionesPorNumero(self.numero)[0]
    # se carga el id de la estacion
    self.id_estacion=estacion[0]
    # se carga el id de laboratorio
    self.id_laboratorio=estacion[1]
    # se carga el numero de la estacion
    self.numero=estacion[2]
     # se carga la ip de la estacion
    self.ip=estacion[3]
    # se carga el usuario de la estacion
    self.username=estacion[4]
     # se carga el password de la estacion
    self.password=estacion[5]

  # Retorna el usuario que esta usando la estacion
  def VerUsuario(self):
    # variable para guardar el usuarios
    usuario=""
    # se verifica si hay conexion
    if(self.maquina_conectada):
    # exepcion en caso de un error inesperado
      try:
        # se conecta via ssh
        self.ssh.Conectar(self.ip,self.username,self.password)
        # se guarda el usuario en la variable usuarios
        usuario= self.ssh.VerUsuario()
        # se cierra la conexion ssh
        self.ssh.CerrarConexion()
      # si ocurre un error
      except Exception:
        # muestra un mensaje de error por consola
        print "( VerUsuario ) Error conectando con la maquina: "+str(self.numero)
    # retorna el resultados
    return usuario
  
  # retorna el tiempo de encendido de la estacion
  def VerTiempoEncendido(self):
    # variable para guardar el tiempo
    tiempo=""
    # exepcion en caso de un error inesperado
    # se verifica si hay conexion
    if(self.maquina_conectada):
      try:
        # se conecta via ssh
        self.ssh.Conectar(self.ip,self.username,self.password)
        # se guarda el usuario en la variable tiempo
        tiempo = self.ssh.VerTiempoEncendido()
        # se cierra la conexion ssh
        self.ssh.CerrarConexion()
      # si ocurre un error
      except Exception:
        # muestra un mensaje de error por consola
        print "( VerTiempoEncendido ) Error conectando con la maquina: "+str(self.numero)
    # retorna el rersultados
    return tiempo

  # retorna la fecha y hora en que se encendio la maquina
  def VerHoraYFechaUsuario(self):
    # variable para guardar el la fecha 
    fechaHora=""
    # exepcion en caso de un error inesperado
    # se verifica si hay conexion
    if(self.maquina_conectada):
      try:
        # se conecta via ssh
        self.ssh.Conectar(self.ip,self.username,self.password)
        # se guarda el la fecha en la variable fecha
        fechaHora= self.ssh.VerHoraYFechaUsuario()
        # se cierra la conexion ssh
        self.ssh.CerrarConexion()
      # si ocurre un error
      except Exception:
        # muestra un mensaje de error por consola
        print "( VerHoraYFechaUsuario ) Error conectando con la maquina: "+str(self.numero)
    # retorna el resultados
    return fechaHora

  # se apaga la estacion
  def Apagar(self):
    # exepcion en caso de un error inesperado
    # se verifica si hay conexion
    if(self.maquina_conectada):
      try:
        # se conecta via ssh
        self.ssh.Conectar(self.ip,self.username,self.password)
        # se apaga la estacion
        self.ssh.ApagarPc()
        # se cierra la conexion ssh
        self.ssh.CerrarConexion()
      # si ocurre un error
      except Exception:
        # muestra un mensaje de error por consola
        print "Error conectando con la maquina: "+str(self.numero)

  # se verifica si la estacion esta conectada
  def Conectada(self):
    # retorna true si la estacion esta conectada y false si no lo esta
    self.maquina_conectada = system("ping -i 0.5 -c 1 -w 1 "+ self.ip +" > ping")==0

  # elimina la estacion
  def EliminarEstacion(self):
    # elimina la etacion por su id
    print "eliminar: "+str(self.id_estacion)
    self.db.DeleteEstacionPorId(self.id_estacion)
    print "eliminar: "+str(self.id_estacion)
  
  # edita una estacion con los datos que hay en la clase
  def EditarEstacion(self):
    # actualiza los datos
    self.db.UpdateEstacionPorSuNumeroYIdLaboratorio(self.id_estacion,self.id_laboratorio_anterior,self.id_laboratorio,self.numero_anterior,self.numero,self.ip,self.username,self.password)

  def GuardarNuevaEstacion(self):
    self.db.InsertEstacionEnLaboratorio(self.id_laboratorio,self.numero,self.ip,self.username,self.password)

