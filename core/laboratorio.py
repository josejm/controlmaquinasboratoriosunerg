# cargo el modelo para realizar consulta con sqlite
from model.model import model
# cargo las clases ssh
from core import ssh
# cargo system para poder hacer ping
from os import system
# cargo la clase para manejar una estaciones
from estacion import estacion

## clase donde se guarda un laboratorio
class lab():
  # id del laboratorio que se asigna en la base de datos
  id_laboratorio=None
  # numero del laboratorios
  numero=None
  # si el laboratorio cuenta con internet
  internet=None
  # si el laboratorio permite invitados
  invitado=None
  # instancia para la clase model
  db= model()
  # instancia para la clase ssh
  ssh=ssh.ssh()
  # todas las estaciones
  estaciones=[]

  # constructor
  def __init__(self,lab):
   if(lab!=None):
     # se busca el laboratorio
     laboratorio=self.db.GetLaboratorioPorNumero(lab)[0]
     # se carga el id del laboratorio
     self.id_laboratorio=laboratorio[0]
     # se carga el numero del laboratorio
     self.numero= laboratorio[1]
     # se carga el estado del internet del laboratorio
     self.internet= laboratorio[2]
     # se carga el estado del invitado
     self.invitado= laboratorio[3]
     # se buscan todas las estaciones del laboratorio
     self.estaciones=[]
     for e in self.db.GetEstacionesPorIdLaboratorio(self.id_laboratorio):
       self.estaciones.append(estacion(e[2]))

  # actualiza los datos
  def ActualizarData(self):
   # se busca el laboratorio
   laboratorio=self.db.GetLaboratorioPorNumero(self.numero)[0]
   # se carga el id del laboratorio
   self.id_laboratorio=laboratorio[0]
   # se carga el numero del laboratorio
   self.numero= laboratorio[1]
   # se carga el estado del internet del laboratorio
   self.internet= laboratorio[2]
   # se carga el estado del invitado
   self.invitado= laboratorio[3]
   # se limpia el array
   self.estaciones=[]
   # se buscan todas las estaciones del laboratorio
   for e in self.db.GetEstacionesPorIdLaboratorio(self.id_laboratorio):
     self.estaciones.append(estacion(e[2]))

  # actualiza los datos
  def BuscarPorId(self,id_lab):
   # se busca el laboratorio
   laboratorio=self.db.GetLaboratorioPorId(id_lab)[0]
   # se carga el id del laboratorio
   self.id_laboratorio=laboratorio[0]
   # se carga el numero del laboratorio
   self.numero= laboratorio[1]
   # se carga el estado del internet del laboratorio
   self.internet= laboratorio[2]
   # se carga el estado del invitado
   self.invitado= laboratorio[3]
   # se limpia el array
   self.estaciones=[]
   # se buscan todas las estaciones del laboratorio
   for e in self.db.GetEstacionesPorIdLaboratorio(self.id_laboratorio):
     self.estaciones.append(estacion(e[2]))

  # Busca todos los usuarios que hay en el laboratori conectado
  def VerUsuarios(self):
    # diccionario donde se guardaran los usuarios
    usuarios={}
    # incia el recorrido por todas las estaciones
    for e in self.estaciones:
       # realiza un ping a la maquina para ver si esta conectada
       if(system("ping -c 1 -w 1"+ e.ip+" > ping")==0):
         # se crea un diccionario y se guarda como llave el numero de la maquna y como valor el usuario que esta en esa maquina
         usuarios[e.numero]=e.VerUsuario()
    # retorna los resultados
    return usuarios


  # retorna un diccionaro con el tiempo de encendido de cada maquina del laboratorio
  def VerTiempoEncendido(self):
    # diccionario donde se guardaran los usuarios
    tiempo={}
    # incia el recorrido por todas las estaciones
    for e in self.estaciones:
       # realiza un ping a la maquina para ver si esta conectada
       if(system("ping -c 1 "+ e.ip+" > ping")==0):
         # se crea un diccionario y se guarda como llave el numero de la maquna y como valor el tiempo que esta en esa maquina
         tiempo[e.numero]=e.VerTiempoEncendido()
    # retorna los resulados
    return tiempo

  # retorna un diccionario con la fecha y hora de encendido de la pc
  def VerHoraYFechaUsuario(self):
    # diccionario donde se guardaran los usuarios
    fechaHora={}
    # incia el recorrido por todas las estaciones
    for e in self.estaciones:
       # realiza un ping a la maquina para ver si esta conectada
       if(system("ping -c 1 "+ e.ip+" > ping")==0):
         # se crea un diccionario y se guarda como llave el numero de la maquna y como valor el tiempo que esta en esa maquina
         fechaHora[e.numero]=e.VerHoraYFechaUsuario()
    # retorna los resulados
    return fechaHora

  #Apaga todas las maquinas
  def ApagarMaquinas(self):
    # incia el recorrido por todas las estaciones
    for e in self.estaciones:
       # realiza un ping a la maquina para ver si esta conectada
       if(e.maquina_conectada):
         # se crea un diccionario y se guarda como llave el numero de la maquna y como valor el tiempo que esta en esa maquina
         e.Apagar()

  # se buscan todas las maquinas conectadas
  def VerMaquinasConectadas(self):
    # un diccionario donde se guardan las maquinas conectadas
    a={}
    # se inicia el recorrido por las estaciones
    for e in self.estaciones:
        # se envia un ping a la maquina y se guarda el resultado en el diccionario . la llave es el numero de la estacion y el valor es el estado (True para conectado , False para no conectado)
        a[e.numero]=e.Conectada()
    # retorna el resultado
    return a

  # desactiva el internet
  def DesactivarInternet(self):
    # se actualiza el estado de internet del laboratorio
    self.db.UpdateLaboratorioPorSuId(self.id_laboratorio,self.numero,0,self.invitado)
    # se actualizan los datos de la clase
    self.ActualizarData()

  # Activa la intenret
  def ActivarInternet(self):
    # se actualzia el estado de al internet del laboratorio
    self.db.UpdateLaboratorioPorSuId(self.id_laboratorio,self.numero,1,self.invitado)
    # se actualizan los datos de la clase
    self.ActualizarData()

  # desactiva el internet
  def DesactivarInvitado(self):
    # se actualiza el estado de sesion de invitado del laboratorio
    self.db.UpdateLaboratorioPorSuId(self.id_laboratorio,self.numero,self.internet,0)
    # se actualizan los datos de la clase
    self.ActualizarData()

  # Activa la intenret
  def ActivarInvitado(self):
    # se actualzia el estado de sesion de invitado del laboratorio
    self.db.UpdateLaboratorioPorSuId(self.id_laboratorio,self.numero,self.internet,1)
    # se actualizan los datos de la clase
    self.ActualizarData()

  # se elimina el laboratorio con sus estaciones
  def EliminarLaboratorio(self):
    # se elimian el laboratorio
    self.db.DeleteLaboratorioPorId(self.laboratorio_id)
    # se elimina las estaciones asociadas a ese laboratorio
    self.db.DeleteEstacionesPorIdLaboratorio(self.laboratorio_id)

  # se elimina una estacion por su numero
  def EliminarEstacionPorNumero(self,numero):
    # se elimina la estacion
    self.db.DeleteEstacionPorNumero(numero)
    # se actualizan los datos
    self.ActualizarData()

  # elimina todas las estaciones del laboratorio
  def ELiminarTodasLasEstaciones(self):
    # recorre todas las estaciones
    for e in self.estaciones:
      # elimina la estacion por su id
      self.db.DeleteEstacionPorId(e[0])
    # actualiza los datos
    self.ActualizarData()

  #Agrega una estaciones
  def AgregarEstacion(self,numero,ip,username,password):
     # se guarda la estacion en la  base de datos
     self.db.InsertEstacionEnLaboratorio(self.id_laboratorio,numero,ip,username,password)



# clase que carga todos los laboratorios
class laboratorios():
  # array para alamcenar todos los objetos tipo lab
  laboratorios=[]
  # instancia para la clase model
  db= model()

  #cosntructor
  def __init__(self):
    laboratorios= self.db.GetLaboratorios()
    print "cantidad:"+str(len(laboratorios))
    self.laboratorios=[]
    for labo in laboratorios:
      self.laboratorios.append(lab(labo[1]))


  #actualiza los datos
  def ActualizarData(self):
    self.laboratorios=[]
    laboratorios= self.db.GetLaboratorios()
    for labo in laboratorios:
      self.laboratorios.append(lab(labo[1]))

  # si no se la pasa un numero se apagaran todas las maquinas de todos los laboratorios
  def ApagarMaquinas(self,numero=-1):
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         laboratorio.ApagarPc()

  # si no se le pasa un numero se obtendran todos los usuarios de todos los laboratorios
  def VerUsuarios(self,numero=-1):
    usuarios={}
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         usuarios[laboratorio.numero]=laboratorio.VerUsuario()
    return usuarios

  # si no se le pasa un numero se obtendra el tiempo de encendido de todas las maquinas de todos los laboratorios
  def VerTiempoEncendido(self,numero=-1):
    tiempo={}
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         tiempo[laboratorio.numero]=laboratorio.VerTiempoEncendido()
    return tiempo

  # si no se le pasa un numero de obtendra la fecha de encendido de todas las maquinas de todos los laboratorios
  def VerFechaEncendido(self,numero=-1):
    fechaHora={}
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         fechaHora[laboratorio.numero]=laboratorio.VerTiempoEncendido()
    return fechaHora

  # si no se la pasa un numero se desactivara la internet en todos lso laboratorios
  def DescativarInternetLaboratorio(self,numero=-1):
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         laboratorio.DesactivarInternet()
         break

  # si no se le pasa un parametro se activara la intenret en todos los laboratorios
  def ActivarInternetLaboratorio(self,numero=-1):
    for laboratorio in self.laboratorios:
       if numero==-1 or laboratorio.numero==numero:
         laboratorio.ActivarInternet()
         break

  # se buscan todas las maquinas conectadas
  def VerMaquinasConectadas(self):
    conectadas={}
    for laboratorio in self.laboratorios:
      conectadas[laboratorio.numero]=laboratorio.VerMaquinasConectadas()
    return conectadas

  # retorna un laboratorio buscandolo por su numeros
  def GetLaboraotorioPorNumero(self,numero):
    for laboratorio in self.laboratorios:
      if laboratorio.numero==numero:
         return laboratorio

  # ELimina un labotarorio por su numero
  def ELiminarLaboratorioPorNumero(self,numnero):
    for laboratorio in self.laboratorios:
      if laboratorio.numero==numero:
         laboratorio.EliminarLaboratorio()
         break
    self.ActualizarData()

  # registra un nuevo laboratorio
  def NuevoLaboratorio(self,numero,internet):
     self.db.InsertLaboratorio(numero,internet)
     self.ActualizarData()













