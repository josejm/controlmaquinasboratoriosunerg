# cargo el modelo para realizar consulta con sqlite
from model.model import model
from core.historial import historial
import conf

class login():
  db= model()
  historia= historial()

  def VerificarLogin(self,user,password,estacion,lab):
    usuario=self.db.BuscarUsuarioPorUserYPassword(user,password)
    if(len(usuario)>0):
      self.historia.usuario= user
      self.historia.laboratorio=lab.numero
      self.historia.maquina=estacion.numero
      self.historia.accion="ENTRO EN LA MAQUINA: "+ str(estacion.numero)
      self.historia.Insertar()
      return True
    return False
