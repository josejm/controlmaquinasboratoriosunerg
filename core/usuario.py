# cargo el modelo para realizar consulta con sqlite
from model.model import model
# cargo las clases ssh
from core import ssh
# cargo system para poder hacer ping
from os import system

class usuario:
  # instancia para la clase model
  db= model()
  id_usuario=0
  user=""
  cedula=""
  password=""
  carpeta=""
  seccion=""



  # Busca un usuario por su user y lo carga en la clase
  def BuscarUsuarioPorUser(self,user):
    usuario=self.db.BuscarUsuarioPorUser(user)[0]
    self.id_usuario=usuario[0]
    self.cedula=usuario[1]
    self.user= usuario[2]
    self.password= usuario[3]
    self.carpeta= usuario[4]
    self.seccion= usuario[5]

  # inserta un nuevo usuario y lo carga en a clase
  def InsertarUsuario(self,user,cedula,password,seccion):
    self.db.InsertUsuario(user,cedula,password,cedula+"_"+user,seccion)
    usuario=self.db.BuscarUsuarioPorUser(user)[0]
    self.id_usuario=usuario[0]
    self.cedula=usuario[1]
    self.user= usuario[2]
    self.password= usuario[3]
    self.carpeta= usuario[4]
    self.seccion= usuario[5]

  def ActualizarUsuario(self):
    self.db.UpdateUsuarioByID(self.id_usuario,self.user,self.cedula,self.password,self.carpeta,self.seccion)

  #ELimina un usuarios
  def EliminarUsuario(self):
    self.db.DeleteUsuaroPorId(self.id_usuario)

class usuarios:
  usuarios=[]
  db= model()

  def __init__(self):
    self.usuarios=[]
    users=self.db.GetUsuarios()
    for user in users:
      u= usuario()
      u.BuscarUsuarioPorUser(user[2])
      self.usuarios.append(u)




