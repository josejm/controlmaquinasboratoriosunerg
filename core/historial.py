# cargo el modelo para realizar consulta con sqlite
from model.model import model
import time
class historial:

  id_historial=0
  usuario=""
  laboratorio=""
  maquina=""
  accion=""
  fecha=None

  def Insertar(self):
    # instancia para la clase model
    db= model()
    db.InsertHistorial(self.usuario,self.laboratorio,self.maquina,self.accion,time.strftime("%y-%m-%d %H:%M:%S"))


class historiales:
  # instancia para la clase model
  db= model()
  historiales=[]

  def BuscarPorLaboratorio(self,laboratorio):
   historiales=self.db.BuscarHistorialPorLaboratorio(laboratorio)
   self._CargarDatos(historiales)

  def BuscarPorUsuario(self,usuario):
   historiales=self.db.BuscarHistorialPorUser(usuario)
   self._CargarDatos(historiales)

  def BuscarPorLaboratorioYMaquina(self,laboratorio,maquina):
   historiales=self.db.BuscarHistorialPorLaboratorioYMaquina(laboratorio,maquina)
   self._CargarDatos(historiales)


  def _CargarDatos(self,historiales):
   self.historiales=[]
   for h in historiales:
      his=historial()
      his.id_historial= h[0]
      his.usuario= h[1]
      his.laboratorio= h[2]
      his.maquina= h[3]
      his.accion= h[4]
      his.fecha= h[5]
      self.historiales.append(his)





