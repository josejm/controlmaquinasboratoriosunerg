from PyQt4 import QtGui
import sys

from core.laboratorio import laboratorios
from core.historial import *
from vistas import PrincipalAdministrador
from controlador import VerLaboratoriosController,EditarAgregarMaquinaController,VerUsuariosController,VerHistorialController,VerPaginasBloqueadasController
import threading
from core.ssh import ssh
from core.usuario import usuarios


class Principal(QtGui.QMainWindow, PrincipalAdministrador.Ui_PrincipalAdministrador):
  lab_seleccionado=None
  labs = None
  threads = None
  threads_stop=None
  ssh= ssh()


  def __init__(self, parent=None):

    super(Principal, self).__init__(parent)
    self.setupUi(self)
    self.btnBuscar.clicked.connect(self.ActualizarDataSubProceso)
    self.btnApagar.clicked.connect(self.ApagarPc)
    self.btnApagarTodas.clicked.connect(self.ApagarTodas)
    self.btnActualizar.clicked.connect(self.ActualizarDataSubProceso)
    self.btnEditar.clicked.connect(self.EditarMaquina)
    self.btnAgregar.clicked.connect(self.AgregarMaquina)
    self.btnEliminar.clicked.connect(self.EliminarMaquina)
    self.btnVerLaboratorios.clicked.connect(self.VerLaboratorios)
    self.btnVerUsuarios.clicked.connect(self.VerUsuarios)
    self.btnVerHistorial.clicked.connect(self.VerHistorial)
    self.btnReiniciarSamba.clicked.connect(self.ReiniciarSamba)
    self.btnEliminarCarpetasCompartidas.clicked.connect(self.EliminarCarpetasCompartidas)
    self.btnReconfigurarProxi.clicked.connect(self.ReconfigurarProxi)
    self.btnVerPaginasBloqueadas.clicked.connect(self.VerPaginas)
    self.ActualizarDataSubProceso()

  def ActualizarDataSubProceso(self):
    if self.threads!=None and self.threads.isAlive():
      self.threads_stop.set()
    self.threads_stop = threading.Event()
    self.threads =threading.Thread(target=self.ActualizarData, args=(1, self.threads_stop))
    self.threads.start();


  def ActualizarData(self,arg1, stop_event):
    self.labs=laboratorios();
    index= self.comboLaboratorios.currentIndex()
    self.comboLaboratorios.clear()
    self.comboLaboratorios.addItem("Todos")
    for lab in self.labs.laboratorios:
      self.comboLaboratorios.addItem(str(lab.numero))
    self.comboLaboratorios.setCurrentIndex(index)
    self.maquinas.setRowCount(0)
    row=0
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        for e in lab.estaciones:
          if stop_event.is_set():
            break
          self.maquinas.insertRow(row)

          numero_lab = QtGui.QTableWidgetItem(str(lab.numero))
          numero = QtGui.QTableWidgetItem(str(e.numero))
          ip = QtGui.QTableWidgetItem(str(e.ip))

          self.maquinas.setItem(row,0, numero_lab)
          self.maquinas.setItem(row,1, numero)
          self.maquinas.setItem(row,2, ip)
          row=row+1
    row=0
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        for e in lab.estaciones:
          if stop_event.is_set():
            break

          e.Conectada()
          conecta = QtGui.QTableWidgetItem(str(e.maquina_conectada))
          self.maquinas.setItem(row,3, conecta)
          row=row+1
    row=0
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        for e in lab.estaciones:
          if stop_event.is_set():
            break

          fechahora = QtGui.QTableWidgetItem(str(e.VerHoraYFechaUsuario()))
          tiempo = QtGui.QTableWidgetItem(str(e.VerTiempoEncendido()))

          self.maquinas.setItem(row,4, fechahora)
          self.maquinas.setItem(row,5, tiempo)
          row=row+1


  def ReconfigurarProxi(self):
    self.ssh.GenerarListaIpsProxi(laboratorios())

  def ReiniciarSamba(self):
    self.ssh.ReiniciarSamba()

  def EliminarCarpetasCompartidas(self):
    self.ssh.EliminarCarpetasCompartidas(usuarios())

  def ApagarPc(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 1).text()
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        for e in lab.estaciones:
          if(str(e.numero)==numero):
            e.Apagar()
            break
    self.ActualizarDataSubProceso()

  def VerHistorial(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 1).text()
    laboratorio= self.maquinas.item(self.maquinas.currentRow(), 0).text()
    h=historiales()
    h.BuscarPorLaboratorioYMaquina(laboratorio,numero)
    VerHistorialController.VerHistorialController(self).main(h)
    

  def EliminarMaquina(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 1).text()
    eliminado=False
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        for e in lab.estaciones:
          if(str(e.numero)==numero):
            e.EliminarEstacion()
            eliminado=True
            break
        if eliminado: break
    self.ActualizarDataSubProceso()

  def ApagarTodas(self):
    for lab in self.labs.laboratorios:
      if self.comboLaboratorios.currentText()=="Todos" or self.comboLaboratorios.currentText()==str(lab.numero):
        lab.ApagarMaquinas()
    self.ActualizarDataSubProceso()

  def EditarMaquina(self):
    numero= self.maquinas.item(self.maquinas.currentRow(), 1).text()
    EditarAgregarMaquinaController.EditarAgregarMaquinaController(self).main(numero)

  def AgregarMaquina(self):
    EditarAgregarMaquinaController.EditarAgregarMaquinaController(self).main(None)

  def VerLaboratorios(self):
    VerLaboratoriosController.VerLaboratoriosController(self).main()

  def VerPaginas(self):
    VerPaginasBloqueadasController.VerPaginasBloqueadasController(self).main()

  def VerUsuarios(self):
    VerUsuariosController.VerUsuariosController(self).main()

def main():
  app = QtGui.QApplication(sys.argv)
  form = Principal()
  form.show()
  app.exec_()


if __name__ == '__main__':
  main()
