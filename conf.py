# -*- coding: utf-8 -*-

print "Aqui solo se guardan las variables de configuracion :)"

# laboratorio para el programa del profesor o para el login del usuario(solo se necesita si se ejecutara el programa del profesor, para el adminsitrador no es necesario ya que este puede ver todos los laboratorios) [el nombre del laboratorio no el id de la base de datos]
numero_lab=2

# variable para la comunicacion con el servidor

IP_SERVER="192.168.0.112"
USER_SERVER="root"
PASSWORD_SERVER="j2017m"

# password del usuario root de esta computadora o algun usuario con permiso para ejcutar mount

PASSWORD_ROOT="j2016m"


# lugar donde se debera de montar la carpeta compartida del servidor

patch_directory_share_local="/home/joser/Documentos/user"

#variables para la conxion con la base de datos

DB_HOST="localhost"
DB_USER="root"
DB_PASSWORD="j2016m"
DB_DATABASE="laboratorio"

# variables para algunos de los comandos que se ejecutaran via ssh 

ssh_cmd_apagar="shutdown -h now"

# iamgen que se muestra en el login del usuario

patch_image="image/logo_unerg.png"

# carpeta o lugar en el servidor donde estan almacenas las carpetas coporatidas

patch_directory_share="/home/jm/Escritorio"

# carpeta donde se almacenaran las configuraciones de de carpetas compartidas de samba
patch_samba_share_directory="/etc/samba/conf_directory_share"

# palabra para diferencias los usuarios del servidor de los usuarios del loginVista

user_magic="jm_"

# configuracion para compartir la carpeta con samba (username : sera remplazado por usuario ,name_directory : sera remplazado por el nombre de la carpeta del usuario)
cmd_samba_share_directory="""
echo \"
[name_directory]
  comment = name_directory
  path = """+patch_directory_share+"""/name_directory
  browseable = yes
  writable = yes
  read only = no
  valid users = """+user_magic+"""username
  workgroup = WORKGROUP
  directory mask = 0777
  create mask = 0777

\" >"""+patch_samba_share_directory+"/username_name_directory.conf.dir"


#comando para generar todos los includes y guardarlos dentro del archivo include en samba

cmd_gen_files_samba="ls "+patch_samba_share_directory+"/*.conf.dir | sed -e 's/^/include = /' > "+patch_samba_share_directory+"/include.conf"

# comando para reiniciar samba_reset
cmd_samba_reset="samba restart"

# comando para eliminar el usuarios

cmd_del_user="userdel "+user_magic+"username"


# conjuntos de comandos para registrar un nuevo usuario en el servidor, crear la carpeta compartida,darle permisos, etc.. (username: se remplaza por el nombre del usuario, password: se reemplaza por el password del usuario)

cmd_directory_share=[
"useradd -M -p password "+user_magic+"username",
"mkdir "+patch_directory_share+"/name_directory",
"chmod 777 -R "+patch_directory_share+"/name_directory",
cmd_samba_share_directory,
cmd_gen_files_samba,
cmd_samba_reset,
"(echo password; echo password) | smbpasswd -a "+user_magic+"username -s"
]


# archivo donde se almacena el usuario que ingreso al sistema

path_tmp_user="/tmp/user.tmp"



print "Para nuevas Versiones https://bitbucket.org/josejm/controlmaquinasboratoriosunerg/overview"




# comando para reconfigurar el proxi (squid3)

cmd_reconfigure="squid3 -k reconfigure"

# patch para el archivo donde estan las declaraciones de los grupos del proxi

patch_proxi_declaraciones_grupos="/etc/squid3/declaraciones.conf"

# patch para el archivo donde estan las reglas (quitar la internet)

patch_proxi_reglas_grupos="/etc/squid3/reglas.conf"

# patch para el archivo donde estan las paginas bloqueadas (cuando se genera se incluye en declaraciones)

patch_proxi_paginas_block="/etc/squid3/block_webs.conf"

# directory donde se guardan las ips de los laboratorios

directory_proxi_ips="/etc/squid3/lab"

# palabra para los archivos donde esta las ips

proxi_magic="jm_"




