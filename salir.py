# -*- coding: utf-8 -*-
import sys,os
from time import sleep
from core.historial import *
import conf
import pexpect
import login

class salir():
  h= historial()
  def __init__(self):
    if os.path.exists(conf.path_tmp):
      arch=open(conf.path_tmp,"r")
      self.h.user=arch.read()
      self.h.laboratorio=conf.numero_lab
      self.h.maquina= conf.numero_maquina
      self.h.accion="EL USUARIO HA CERRADO SECCION EN LA MAQUINA: "+str(conf.numero_maquina)
      self.h.Insertar()
      os.system("rm "+conf.path_tmp)
      self.CerrarCarpetaCompartida()
      login.main()

  def CerrarCarpetaCompartida(self):
    cmd = pexpect.spawn("su")
    cmd.expect ('.') 
    cmd.sendline(str(conf.PASSWORD_ROOT))
    cmd.sendline("umount "+conf.patch_directory_share_local)
    cmd.sendline("exit")
    cmd.close()

salir()

