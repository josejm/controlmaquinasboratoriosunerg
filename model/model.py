# se importa la clase DataBase del archivo database
from database import DataBase
import hashlib

# clase donde se guardaran los modelos
class model():
  # conexion con la base de datos squlite
  db= DataBase()
  
  # cosntructor
  def __init__(self):
   pass

  # retorna todos los laboratorios
  def GetLaboratorios(self):
   return self.db.executeSql("SELECT * FROM laboratorio")

  # retorna un labotorio por su id
  def GetLaboratorioPorId(self,ID):
   return self.db.executeSql("SELECT * FROM laboratorio WHERE id="+str(ID))

  # retorna un laboratorio por su numero
  def GetLaboratorioPorNumero(self,numero):
   return self.db.executeSql("SELECT * FROM laboratorio WHERE numero="+str(numero))

  # retorna todas las estaciones
  def GetEstaciones(self):
   return self.db.executeSql("SELECT * from estacion")

  # retorna todas las estaciones
  def GetEstacionesPorIP(self,ip):
   return self.db.executeSql("SELECT * from estacion WHERE ip='"+str(ip)+"'")

  # retorna una estacion por su id
  def GetEstacionesPorID(self,ID):
   return self.db.executeSql("SELECT * from estacion WHERE id="+str(ID))

  # retorna una estacion por su numero
  def GetEstacionesPorNumero(self,numero):
   return self.db.executeSql("SELECT * from estacion WHERE numero="+str(numero))

  # retorna todas las estaciones de un laboratorio
  def GetEstacionesPorIdLaboratorio(self,laboratorio_id):
   return self.db.executeSql("SELECT estacion.* FROM estacion WHERE id_laboratorio="+str(laboratorio_id))

  # elimina una estacion por su id
  def DeleteEstacionPorId(self,ID):
    return self.db.executeSql("DELETE FROM estacion WHERE id='"+str(ID)+"'",True)

  # elimina una estacion por su numero
  def DeleteEstacionPorNumero(self,numero):
    return self.db.executeSql("DELETE FROM estacion WHERE numero="+str(numero),True)

  # elimina todas las estaciones de un laboratorio
  def DeleteEstacionesPorIdLaboratorio(self,laboratorio_id):
    return self.db.executeSql("DELETE FROM estacion WHERE id_laboratorio="+str(laboratorio_id),True)

  # elimina un laboratorio por su id
  def DeleteLaboratorioPorId(self,ID):
    return  self.db.executeSql("DELETE FROM laboratorio WHERE id="+str(ID),True)

  # Actualiza una esacion por su id
  def UpdateEstacionPorSuNumeroYIdLaboratorio(self,ID,laboratorio_id_anterior,laboratorio_id,numero_anterior,numero,ip,username,password):
   return self.db.executeSql("UPDATE estacion SET id_laboratorio='"+str(laboratorio_id)+"', numero='"+str(numero)+"', ip='"+str(ip)+"', username='"+str(username)+"', password='"+str(password)+"' WHERE numero='"+str(numero_anterior)+"' AND id_laboratorio='"+str(laboratorio_id_anterior)+"'",True)

  # actuacliza un alboratorio por su id
  def UpdateLaboratorioPorSuId(self,ID,numero,internet,invitado):
    return self.db.executeSql("UPDATE laboratorio SET numero='"+str(numero)+"', internet='"+str(internet)+"', invitado='"+str(invitado)+"' WHERE id="+str(ID),True)

  # inserta una nueva estacion en un laboratorio
  def InsertEstacionEnLaboratorio(self,laboratorio_id,numero,ip,username,password):
    return self.db.executeSql("INSERT INTO `estacion` ( `id_laboratorio`, `numero`, `ip`, `username`, `password`) VALUES('"+str(laboratorio_id)+"', '"+str(numero)+"', '"+str(ip)+"', '"+str(username)+"', '"+str(password)+"');",True)

  # inserta un laboratorio
  def InsertLaboratorio(self,numero,internet):
    return self.db.executeSql("INSERT INTO `laboratorio` (`numero`, `internet`) VALUES('"+str(numero)+"','"+str(internet)+"');",True)

  # busca un usuario por su usd
  def BuscarUsuarioPorUser(self,user):
   return self.db.executeSql("SELECT * from usuario WHERE user='"+str(user)+"'")

  # Buscar un usuario por el user y el password
  def BuscarUsuarioPorUserYPassword(self,user,password):
   m = hashlib.md5()
   m.update(password)
   return self.db.executeSql("SELECT * from usuario WHERE user='"+str(user)+"' AND password='"+str(m.hexdigest())+"'")

  # busca todos los usuarios
  def GetUsuarios(self):
   return self.db.executeSql("SELECT * from usuario")

  # inserta un usuario
  def InsertUsuario(self,user,cedula,password,carpeta,seccion):
    m = hashlib.md5()
    m.update(password)
    return self.db.executeSql("INSERT INTO `usuario` (`user`,`cedula`,`password`,`carpeta`,`seccion`) VALUES('"+str(user)+"','"+str(cedula)+"','"+str(m.hexdigest())+"','"+str(carpeta)+"','"+str(seccion)+"');",True)

  # elimina un usuario por su id
  def DeleteUsuaroPorId(self,ID):
    return  self.db.executeSql("DELETE FROM usuario WHERE id="+str(ID),True)

  #Actualiza un usuarios
  def UpdateUsuarioByID(self,ID,user,cedula,password,carpeta,seccion):
   m = hashlib.md5()
   m.update(password)
   return self.db.executeSql("UPDATE usuario SET cedula='"+str(cedula)+"',user='"+str(user)+"', password='"+str(m.hexdigest())+"', carpeta='"+str(carpeta)+"', seccion='"+str(seccion)+"' WHERE id="+str(ID),True)

  def InsertHistorial(self,usuario,laboratorio,maquina,accion,fecha):
    return self.db.executeSql("INSERT INTO `historial_usuario` (`usuario`,`laboratorio`,`maquina`,`accion`,`fecha`) VALUES('"+str(usuario)+"','"+str(laboratorio)+"','"+str(maquina)+"','"+str(accion)+"','"+str(fecha)+"');",True)

  def BuscarHistorialPorUser(self,usuario):
   return self.db.executeSql("SELECT * FROM historial_usuario WHERE usuario='"+str(usuario+"'"))

  def BuscarHistorialPorLaboratorio(self,laboratorio):
   return self.db.executeSql("SELECT * FROM historial_usuario WHERE laboratorio='"+str(laboratorio+"'"))

  def BuscarHistorialPorLaboratorioYMaquina(self,laboratorio,maquina):
   return self.db.executeSql("SELECT * FROM historial_usuario WHERE maquina='"+str(maquina)+"' AND laboratorio='"+str(laboratorio)+"'")

  def InsertPagina(self,pagina):
    self.db.executeSql("INSERT INTO block_web (web) VALUES('"+str(pagina)+"')",True)

  def GetPaginas(self):
    return self.db.executeSql("SELECT * from block_web")

  def DeletePaginaPorWeb(self,pagina):
    self.db.executeSql("DELETE FROM block_web WHERE web='"+str(pagina)+"'",True)









