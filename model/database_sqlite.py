# se import sqlite3 para poder manejar base de datos sqlites
import sqlite3
# se importa os para poder interactuar con el sistema, en este caso para verificar si existe un archivo
import os

# clase para crear la base de datos squilite
class DataBase():
  # variable donde se almacena la conexion
  bd=None
  # nombre del archivo sqlite
  nombre_base_datos="model/laboratorio.db"
  # archivo sql con que se creara la banse de datos
  archivo_sql="model/laboratorio.sql"

  # constructor
  def __init__(self):
    # si la base de datos no existe se crea a partir de una archivo sql
    if not os.path.exists(self.nombre_base_datos):
      # se crea la base de datos
      self.bd = sqlite3.connect(self.nombre_base_datos)
      # se abre el archivo sql
      estructura_base_datos=open(self.archivo_sql)
      # se inicializa la variable sql
      sql=""
      # se cre aun bucle para recorrer linea por linea el archivo sql
      for linea in estructura_base_datos.readlines():
           # se ejecuta las lineas sel archivo sql en la base de datos sqlite
           self.bd.execute(linea)
      # se cierra el archivo sql
      estructura_base_datos.close()
    # si la base de datos existe se abre la conexion
    else:
      # se abre la conexion con la  base de datos
      self.bd= sqlite3.connect(self.nombre_base_datos)

  # se ejecuta un sql y retorna el resultado
  def executeSql(self, sql,guardar=False):

    # si se ejecutara un sql y se guardara el cambio
    if guardar:
      # se ejecuta el sql
      self.bd.execute(sql)
      # se guarda los cambios
      self.bd.commit()
    # si no solo se ejecuta y retorna los resultados
    else:
    # retorna el resultado del sql
      return self.bd.execute(sql)


