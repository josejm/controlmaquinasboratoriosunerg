# se import sqlite3 para poder manejar base de datos mysql
import conf
import MySQLdb

# clase para crear la base de datos squilite
class DataBase():
  # variable donde se almacena la conexion
  bd=None
  servidor=conf.DB_HOST
  user=conf.DB_USER
  password=conf.DB_PASSWORD
  basedatos=conf.DB_DATABASE
  datos=[]
  cursor=None
  # constructor
  def __init__(self):
    self.datos = [self.servidor, self.user,self.password,self.basedatos] 

  # se ejecuta un sql y retorna el resultado
  def executeSql(self, sql,guardar=False):
    self.bd = MySQLdb.connect(*self.datos) # Conectar a la base de datos 
    self.cursor = self.bd.cursor()         # Crear un cursor 
    res=None
    # si se ejecutara un sql y se guardara el cambio
    if guardar:
      # se ejecuta el sql
      self.cursor.execute(sql)
      # se guarda los cambios
      self.bd.commit()
    # si no solo se ejecuta y retorna los resultados
    else:
    # retorna el resultado del sql
      self.cursor.execute(sql)
      res=self.cursor.fetchall()
    self.cursor.close()
    self.bd.close()
    return res


